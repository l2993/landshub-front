/* This example requires Tailwind CSS v2.0+ */

import { useEffect, useState } from 'react';

import Booking from '../models/booking.model';
import Toastr from './Toastr';

type Props = {
    bookings: Booking[] | null;
    startingTimeMorning: number;
    endingTimeMorning: number;
    startingTimeAfternoon: number;
    endingTimeAfternoon: number;
    book: (selectedBookings: number) => void;
};

export default function CalendarComponent({ bookings, startingTimeMorning, endingTimeMorning, startingTimeAfternoon, endingTimeAfternoon, book }: Props) {
    const [schedule, setSchedule] = useState<JSX.Element[]>();
    const [bookedSchedule, setBookedSchedule] = useState<JSX.Element[]>();
    const [selectedBookingsIndex, setSelectedBookingsIndex] = useState<number[]>([]);
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });

    const landSchedule = () => {
        const schedule = [];

        for (let index = startingTimeMorning; index < endingTimeAfternoon + 1; index++) {
            schedule.push(
                <div>
                    <div className="sticky left-0 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">{index}H</div>
                </div>,
            );
        }

        setSchedule(schedule);
    };

    const clickOnBooking = (booking: number): void => {
        if (selectedBookingsIndex.length > 3 && !selectedBookingsIndex.includes(booking)) {
            setToastr({ type: 'info', title: 'Limite de créneaux atteinte ', message: 'Vous avez déjà sélectionner 4 créneaux' });
            setShowToastr(true);
            setTimeout(() => setShowToastr(false), 3000);
        } else {
            if (selectedBookingsIndex.includes(booking)) {
                const tps_indexes = [...selectedBookingsIndex];
                setSelectedBookingsIndex(tps_indexes.filter((i: number) => i !== booking));
            } else {
                setSelectedBookingsIndex([...selectedBookingsIndex, booking]);
            }
            book(booking);
        }
    };

    const bookingsScheduled = (): void => {
        const bookingsScheduled = [];

        if (bookings) {
            for (let index = startingTimeMorning; index < endingTimeAfternoon + 1; index++) {
                const booking = bookings.find((booking: Booking) => {
                    const dateBooking = new Date(booking.bookedTime);
                    return dateBooking.getUTCHours() === index && !booking.isCancelled;
                });

                if (booking) {
                    bookingsScheduled.push(
                        <li key={index.toString()} className="relative mt-px flex" style={{ gridRow: index - startingTimeMorning + 2 + ' / span 1' }}>
                            <a className="group absolute inset-1 flex flex-col overflow-y-auto rounded-lg p-2 text-sm leading-5 bg-pink-100 hover:bg-pink-200">
                                <p className="order-1 font-semibold text-pink-700">Réservé</p>
                            </a>
                        </li>,
                    );
                } else if ((index < startingTimeAfternoon && index < endingTimeMorning) || (index > startingTimeAfternoon + 1 && index > endingTimeMorning && index !== endingTimeAfternoon)) {
                    bookingsScheduled.push(
                        <li key={index.toString()} className="relative mt-px flex cursor-pointer" style={{ gridRow: index - startingTimeMorning + 2 + ' / span 1' }}>
                            <a
                                id={index.toString()}
                                onClick={() => clickOnBooking(index)}
                                className={
                                    (selectedBookingsIndex.includes(index) ? 'bg-green-100 hover:bg-green-200 text-green-700' : 'bg-blue-100 hover:bg-blue-200 text-blue-700') +
                                    ' group absolute inset-1 flex flex-col overflow-y-auto rounded-lg p-2 text-sm leading-5'
                                }
                            >
                                <p id={'p' + index.toString()} className="order-1 font-semibold">
                                    Disponible
                                </p>
                            </a>
                        </li>,
                    );
                }
            }
        }

        setBookedSchedule(bookingsScheduled);
    };

    useEffect(() => {
        landSchedule();
        bookingsScheduled();
    }, [bookings, startingTimeMorning, endingTimeMorning, startingTimeAfternoon, endingTimeAfternoon]);

    useEffect(() => {
        bookingsScheduled();
    }, [selectedBookingsIndex]);

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            <div className="flex w-full flex-auto pr-10">
                <div className="w-14 flex-none bg-white ring-1 ring-gray-100" />
                <div className="grid flex-auto grid-cols-1 grid-rows-1">
                    {/* Horizontal lines */}
                    <div className="col-start-1 col-end-2 row-start-1 grid divide-y divide-gray-100" style={{ gridTemplateRows: 'repeat(' + (endingTimeAfternoon - startingTimeMorning + 1) + ', minmax(3.5rem, 1fr))' }}>
                        <div className="row-end-1 h-7"></div>
                        {schedule}
                    </div>

                    {/* Events */}
                    <ol className="col-start-1 col-end-2 row-start-1 grid grid-cols-1" style={{ gridTemplateRows: '1.75rem repeat(' + (endingTimeAfternoon - startingTimeMorning + 1) + ', minmax(0, 1fr)) auto' }}>
                        {bookedSchedule}
                    </ol>
                </div>
            </div>
        </>
    );
}
