import { AuthContext, AuthContextType } from './auth.context';
import React, { ReactNode, useContext } from 'react';

import LandSchedule from '../models/landSchedule.model';

export type LandContextType = {
    getLandSchedules: (land: string) => Promise<LandSchedule[] | null>;
};

export const LandContext = React.createContext<LandContextType | null>(null);

export const LandProvider = ({ children }: { children: ReactNode }) => {
    const { token } = useContext(AuthContext) as AuthContextType;

    const getLandSchedules = async (land: string): Promise<LandSchedule[] | null> => {
        try {
            const url = new URL(`${process.env.REACT_APP_API_URL}/land_schedules/land/` + land);

            const response: Response = await fetch(url.toString(), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            });

            const json = await response.json();

            if (json.code && json.code === 401) {
                return null;
            } else {
                return json;
            }
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    return (
        <LandContext.Provider
            value={{
                getLandSchedules,
                // createLandSchedule,
            }}
        >
            {children}
        </LandContext.Provider>
    );
};
