import jwt_decode from 'jwt-decode';
import lodash from 'lodash';

//Verifie si on est admin
export const isAdmin = (): boolean => {
    try {
        const token = localStorage.getItem('token');
        if (!token) {
            return false;
        }
        const decoded: { roles: string[] } = jwt_decode(token);
        if (!decoded) {
            return false;
        }

        const find = lodash.find(decoded.roles, (role: string) => {
            return role == 'ROLE_ADMIN';
        });
        if (find) {
            return true;
        }
        return false;
    } catch (error) {
        return false;
    }
};

//Verifie si on est MANAGER
export const isManager = (): boolean => {
    try {
        const token = localStorage.getItem('token');
        if (!token) {
            return false;
        }
        const decoded: { roles: string[] } = jwt_decode(token);
        if (!decoded) {
            return false;
        }
        const find = lodash.find(decoded.roles, (role: string) => {
            return role == 'ROLE_MANAGER';
        });
        if (find) {
            return true;
        }
        return false;
    } catch (error) {
        return false;
    }
};

//Verifie si on est Player
export const isPlayer = (): boolean => {
    try {
        const token = localStorage.getItem('token');
        if (!token) {
            return false;
        }
        const decoded: { roles: string[] } = jwt_decode(token);
        if (!decoded) {
            return false;
        }
        const find = lodash.find(decoded.roles, (role: string) => {
            return role == 'ROLE_PLAYER';
        });
        if (find) {
            return true;
        }
        return false;
    } catch (error) {
        return false;
    }
};

//Vérifie si l'utilisateur est connecté
export const isAuthenticated = (): boolean => {
    try {
        const token = localStorage.getItem('token');
        if (!token) {
            return false;
        }
        const decoded: { roles: string[] } = jwt_decode(token);
        if (decoded) {
            return true;
        }
        return false;
    } catch (error) {
        return false;
    }
};

export const getToken = (): string => {
    return 'Bearer ' + localStorage.getItem('token');
};

//Renvoie le nom d'utilisateur de la personne connecté
export const getUserAuth = (): string => {
    try {
        const token = localStorage.getItem('token');
        if (token) {
            return token;
        }
        return '';
    } catch (error) {
        return '';
    }
};

//Renvoie l'id de l'utilisateur connecté
export const getUserIDAuth = () => {
    try {
        const token = localStorage.getItem('token');
        if (token) {
            const decoded: { roles: string[]; id: number } = jwt_decode(token);
            if (decoded) {
                return decoded.id;
            } else {
                return '';
            }
        }
    } catch (error) {
        return '';
    }
};

//Renvoie l'url de la photo
export const getProfilPicture = () => {
    try {
        const token = localStorage.getItem('token');
        if (token) {
            const decoded: { roles: string[]; id: number; avatar: string } = jwt_decode(token);
            if (decoded) {
                return decoded.avatar;
            } else {
                return '';
            }
        }
    } catch (error) {
        return '';
    }
};
