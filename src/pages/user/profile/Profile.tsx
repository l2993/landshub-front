import jwtDecode from 'jwt-decode';
import moment from 'moment';
import { useContext, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Badge from '../../../components/Badge';
import LoaderFetching from '../../../components/LoaderFetching';
import Toastr from '../../../components/Toastr';
import { AuthContext, AuthContextType } from '../../../contexts/auth.context';
import { UserContext, UserContextType } from '../../../contexts/user.context';
import DecodedToken, { emptyDecodedToken } from '../../../models/decodedToken.model';
import User from '../../../models/user.model';

const roles = {
    ROLE_MANAGER: 'Manager',
    ROLE_PLAYER: 'Joueur/Joueuse',
};

export default function Profile(): JSX.Element {
    const { getProfile, getUser, deleteProfile } = useContext(UserContext) as UserContextType;
    const { token, logout } = useContext(AuthContext) as AuthContextType;
    const [user, setUser] = useState<User>();
    const [decodedToken, setDecodedToken] = useState<DecodedToken>(emptyDecodedToken);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });
    const params = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        setIsLoading(true);
        setDecodedToken(token === '' ? emptyDecodedToken : jwtDecode(token));
        async function fetchUser() {
            if (params['id']) {
                const u = await getUser(params['id']);
                if (u) setUser(u);
            } else {
                const u = await getProfile();
                if (u) setUser(u);
            }
            setIsLoading(false);
        }
        fetchUser();
    }, [params]);

    const onDeleteProfile = async (): Promise<void> => {
        const isDelete = await deleteProfile();
        if (isDelete) {
            logout();
            setToastr({ type: 'success', title: 'Profil supprimé avec succès', message: '' });
            setShowToastr(true);
            setTimeout(() => navigate('/login'), 1500);
        } else {
            setToastr({ type: 'error', title: 'Erreur inattentue', message: "Le profil n'a pas pu être supprimé" });
            setShowToastr(true);
        }
    };

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            {isLoading ? (
                <LoaderFetching text={'Récupération du profil ...'} />
            ) : (
                <div className="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div className="flex flex-col md:flex-row p-4 sm:px-6 items-center">
                        <div className="flex flex-col md:flex-row w-full justify-between">
                            <div className="flex gap-x-5 items-center">
                                <div>
                                    <img className="h-16 w-16 rounded-full" src={user?.avatarUrl} />
                                </div>
                                <div>
                                    <h3 className="text-lg leading-6 font-medium text-gray-900">
                                        {user?.firstname} {user?.lastname}
                                    </h3>
                                    <p className="mt-1 max-w-2xl text-sm text-gray-500">
                                        {user?.roles
                                            .map((role: string, index: number, array: string[]) => {
                                                if (role === 'ROLE_MANAGER') return roles.ROLE_MANAGER;
                                                else if (role === 'ROLE_PLAYER') return roles.ROLE_PLAYER;
                                                else if (role === 'ROLE_USER') array.splice(index);
                                            })
                                            .join(', ')}
                                    </p>
                                </div>
                            </div>
                            {user?.id === decodedToken?.id && (
                                <div className="flex p-2 gap-x-2">
                                    <Link to="/profile/edit" className="bg-indigo-700 text-white border-indigo-700 rounded shadow p-2">
                                        Modifier le profil
                                    </Link>
                                    <div className="bg-red-500 text-white border-red-500 rounded shadow p-2" onClick={() => onDeleteProfile()}>
                                        Supprimer le profil
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className="border-t border-gray-200">
                        <dl>
                            <div className="bg-gray-50 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt className="text-sm font-medium text-gray-500">Adresse mail</dt>
                                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{user?.email}</dd>
                            </div>
                            <div className="bg-gray-100 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt className="text-sm font-medium text-gray-500">Numéro de téléphone</dt>
                                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{user?.phone}</dd>
                            </div>
                            {user?.id === decodedToken?.id && (
                                <div className="bg-gray-50 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt className="text-sm font-medium text-gray-500">Profil public</dt>
                                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        <Badge textColor={'text-white'} backgroundColor={user?.isPublic ? 'bg-green-500' : 'bg-red-500'} label={user?.isPublic ? 'Oui' : 'Non'} />
                                    </dd>
                                </div>
                            )}
                            <div className="bg-gray-100 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt className="text-sm font-medium text-gray-500">Mes sports</dt>
                                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{user?.userSports?.map(({ sport, level }: { sport: { name: string }; level: string }) => `${sport.name} (${level})`).join(', ')}</dd>
                            </div>
                            <div className="bg-gray-50 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt className="text-sm font-medium text-gray-500">Dernière modification le</dt>
                                <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{moment(user?.updatedAt).format('DD/MM/yyyy HH:mm')}</dd>
                            </div>
                            {decodedToken.roles.includes('ROLE_PLAYER') && (
                                <div className="bg-gray-100 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt className="text-sm font-medium text-gray-500">Amis</dt>
                                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        {user?.friends?.some((friend: User) => friend.isPublic) ? (
                                            <ul role="list" className="border border-gray-200 rounded-md divide-y divide-gray-200">
                                                {user?.friends?.map((friend: User) => {
                                                    if (!friend.isPublic) return;
                                                    return (
                                                        <li key={friend.id} className="pl-3 pr-4 py-3 flex items-center justify-between text-sm">
                                                            <div className="w-0 flex-1 flex items-center">
                                                                <span className="ml-2 flex-1 w-0 truncate">
                                                                    {friend?.firstname} {friend?.lastname}
                                                                </span>
                                                            </div>
                                                            <div className="ml-4 flex-shrink-0">
                                                                <Link to={'/profile/' + friend?.id} className="font-medium text-indigo-600 hover:text-indigo-500">
                                                                    Voir le profil
                                                                </Link>
                                                            </div>
                                                        </li>
                                                    );
                                                })}
                                            </ul>
                                        ) : (
                                            <span>Vous n&apos;avez aucun ami !</span>
                                        )}
                                    </dd>
                                </div>
                            )}
                            {user?.id === decodedToken?.id && (
                                <div className="bg-gray-50 p-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt className="text-sm font-medium text-gray-500">Actions</dt>
                                    <dd className="mt-2 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        {user?.roles.includes('ROLE_PLAYER') && (
                                            <Link className="bg-primary text-white border-primary rounded shadow p-2" to="/complexes">
                                                Mes réservations
                                            </Link>
                                        )}
                                        {user?.roles.includes('ROLE_MANAGER') && (
                                            <Link className="bg-primary text-white border-primary rounded shadow p-2" to="/complexes">
                                                Mes complexes
                                            </Link>
                                        )}
                                    </dd>
                                </div>
                            )}
                        </dl>
                    </div>
                </div>
            )}
        </>
    );
}
