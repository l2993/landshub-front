import { useContext, useEffect, useState } from 'react';
import makeAnimated from 'react-select/animated';
import { Link } from 'react-router-dom';
import { ChevronRightIcon, MailIcon } from '@heroicons/react/solid';
import User from '../../models/user.model';
import { UserContext, UserContextType } from '../../contexts/user.context';
import Select from 'react-select';
import { SportContext, SportContextType } from '../../contexts/sport.context';
import LoaderFetching from '../../components/LoaderFetching';
import Pagination from '../../components/Pagination';

const animatedComponents = makeAnimated();

export default function ListPlayers(): JSX.Element {
    const pageSize = 8;
    const [players, setPlayers] = useState<User[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [search, setSearch] = useState<string>('');
    const [sports, setSports] = useState<{ id: number; name: string }[]>([]);
    const [sportsSelected, setSportsSelected] = useState<{ value: number; label: string }[]>([]);

    const { getPlayers } = useContext(UserContext) as UserContextType;
    const { getSports } = useContext(SportContext) as SportContextType;

    // Page actuelle
    const [page, setPage] = useState<number>(1);
    //Indique si la pagination est necessaire ou non par un boolean
    const [paginate, setPaginate] = useState<boolean>(false);
    const [maxPage, setMaxPage] = useState<number>(1);

    useEffect(() => {
        setIsLoading(true);
        async function fetchPlayers(): Promise<void> {
            const p = await getPlayers(page, search, sportsSelected);
            if (p) {
                setPlayers(p.users);
                setUpPagination(p);
            }
            setIsLoading(false);
        }

        async function fetchSports(): Promise<void> {
            const s = await getSports();
            setSports(s ?? []);
        }

        fetchSports();
        fetchPlayers();
    }, []);

    useEffect(() => {
        setIsLoading(true);
        async function fetchPlayers(): Promise<void> {
            const p = await getPlayers(page, search, sportsSelected);
            if (p) {
                setPlayers(p.users);
                setUpPagination(p);
            }
            setIsLoading(false);
        }
        fetchPlayers();
        setIsLoading(false);
    }, [sportsSelected, search, page]);

    const handleChangeSports = (newValue: unknown): void => {
        setSportsSelected(newValue as { value: number; label: string }[]);
    };

    const setUpPagination = (c: { users: User[]; totalCount: number }): void => {
        if (c?.users.length < c?.totalCount) {
            setPaginate(true);
            setMaxPage(Math.ceil(c?.totalCount / pageSize));
        } else {
            setPaginate(false);
        }
    };

    //Fonction pour passer à la page suivante
    function next(): void {
        setPage((page) => Math.min(page + 1, maxPage));
    }

    //Fonction pour passer à la page précédente
    function prev(): void {
        setPage((page) => Math.max(page - 1, 1));
    }

    //Fonction pour passer à une page indiqué
    function jump(pageNb: number): void {
        const pageNumber = Math.max(1, pageNb);
        setPage(() => Math.min(pageNumber, maxPage));
    }

    return (
        <>
            {isLoading ? (
                <LoaderFetching text={'Récupération des informations des joueurs...'} />
            ) : (
                <div className="mx-8 md:mx-16">
                    <div className="mt-8 bg-white overflow-hidden shadow rounded-lg">
                        <div className="m-4 flex gap-6 flex-col md:flex-row md:items-center">
                            <input
                                type="search"
                                id="search"
                                placeholder="Rechercher"
                                className="border border-gray-300 bg-gray-200 p-2 rounded-md shadow-xl"
                                onBlur={(event: React.ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
                            />
                            <div>
                                <Select
                                    placeholder="Choisir un sport"
                                    closeMenuOnSelect={false}
                                    components={animatedComponents}
                                    isMulti
                                    options={sports.map((sport: { id: number; name: string }) => {
                                        return { value: sport.id, label: sport.name };
                                    })}
                                    onChange={handleChangeSports}
                                />
                            </div>
                        </div>
                        <div className="bg-white shadow overflow-hidden sm:rounded-md">
                            <ul role="list" className="divide-y divide-gray-200">
                                {players.map((player) => (
                                    <li key={player.id}>
                                        <Link to={`/profile/${player.id}`} className="block hover:bg-gray-50">
                                            <div className="flex items-center px-4 py-4 sm:px-6">
                                                <div className="min-w-0 flex-1 flex items-center">
                                                    <div className="flex-shrink-0">
                                                        <img className="h-12 w-12 rounded-full" src={player.avatarUrl} alt="" />
                                                    </div>
                                                    <div className="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4">
                                                        <div>
                                                            <p className="text-sm font-medium text-indigo-600 truncate">{player.firstname + ' ' + player.lastname}</p>
                                                            <p className="mt-2 flex items-center text-sm text-gray-500">
                                                                <MailIcon className="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" aria-hidden="true" />
                                                                <span className="truncate">{player.email}</span>
                                                            </p>
                                                        </div>
                                                        <div className="hidden md:block">
                                                            <div>
                                                                <p className="text-sm text-gray-900">{player?.userSports?.map(({ sport, level }: { sport: { name: string }; level: string }) => `${sport.name} (${level})`).join(', ')}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <ChevronRightIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                                                </div>
                                            </div>
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <Pagination currentPage={page} next={next} prev={prev} jump={jump} paginate={paginate} maxPage={maxPage} />
                    </div>
                </div>
            )}
        </>
    );
}
