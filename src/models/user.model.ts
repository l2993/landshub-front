export default interface User {
    id: number;
    firstname: string;
    lastname: string;
    phone: string;
    email: string;
    roles: Array<string>;
    createdAt: string;
    updatedAt: string;
    isPublic: boolean;
    avatarUrl: string;
    userSports?: {
        sport: {
            name: string;
        };
        level: string;
    }[];
    friends?: User[];
}

export interface UserRegister {
    firstName: string;
    lastName: string;
    phone: string;
    email: string;
    isPublic: boolean;
    password: string;
    roles: Array<string>;
}

export interface ProfileRegister {
    firstname: string;
    lastname: string;
    phone: string;
    email: string;
    isPublic: boolean;
    avatarUrl: string;
}

export interface UserLogin {
    username: string;
    password: string;
}
