import React, { ReactNode } from 'react';

export type SportContextType = {
    getSports: () => Promise<{ id: number; name: string }[] | null>;
};

export const SportContext = React.createContext<SportContextType | null>(null);

export const SportProvider = ({ children }: { children: ReactNode }) => {
    const getSports = async (): Promise<{ id: number; name: string }[] | null> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/sports`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            });

            const json: { id: number; name: string }[] = await response.json();

            return json;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    return (
        <SportContext.Provider
            value={{
                getSports,
            }}
        >
            {children}
        </SportContext.Provider>
    );
};
