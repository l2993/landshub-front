import React, { useContext, useState } from 'react';
import { Switch } from '@headlessui/react';
import { UserRegister } from '../../models/user.model';
import { Link, useNavigate } from 'react-router-dom';
import { AuthContext, AuthContextType } from '../../contexts/auth.context';
import Toastr from '../../components/Toastr';
import lodash from 'lodash';

export default function Register(): JSX.Element {
    const { register } = useContext(AuthContext) as AuthContextType;
    const navigate = useNavigate();

    const [user, setUser] = useState<UserRegister>({
        email: '',
        firstName: '',
        isPublic: false,
        lastName: '',
        password: '',
        phone: '',
        roles: ['ROLE_PLAYER'],
    });
    const [passwordEquals, setPasswordEquals] = useState<boolean>(false);
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });

    const onSubmit = async (): Promise<void> => {
        if (formIsValid()) {
            const res = await register(user);
            if (res.email) {
                setToastr({ type: 'success', title: 'Enregistrement réussi', message: 'Vous êtes maintenant inscrit' });
            } else {
                setToastr({ type: 'error', title: "L'enregistrement à échoué", message: 'Une erreur est survenue' });
            }
            setShowToastr(true);

            if (res.email) {
                setTimeout(() => navigate('/login'), 1500);
            }
        }
    };

    const checkPasswordConfirmation = (passwordConfirmation: string): void => {
        if (user.password === passwordConfirmation && user.password.length > 0) {
            setPasswordEquals(true);
        }
    };

    const formIsValid = (): boolean => {
        const { firstName, lastName, email } = user;
        return firstName.length > 0 && lastName.length > 0 && email.length > 0 && passwordEquals;
    };

    const onChangeRole = (): void => {
        const tps_user = lodash.cloneDeep(user);
        if (isPlayer(tps_user)) {
            tps_user.roles.splice(tps_user.roles.indexOf('ROLE_PLAYER'), 1);
            tps_user.roles.push('ROLE_MANAGER');
        } else {
            tps_user.roles.splice(tps_user.roles.indexOf('ROLE_MANAGER'), 1);
            tps_user.roles.push('ROLE_PLAYER');
        }
        setUser(tps_user);
    };

    const isPlayer = (user: UserRegister): boolean => {
        return user.roles.includes('ROLE_PLAYER');
    };
    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            <div className="mx-auto px-4 h-full my-6">
                <div className="flex content-center items-center justify-center h-full">
                    <div className="w-full lg:w-8/12 px-4">
                        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                            <div className="rounded-t mb-0 px-6 py-6">
                                <div className="text-center mb-3">
                                    <h2 className="text-primary text-2xl font-bold">Inscription</h2>
                                </div>
                                <form className="flex flex-col space-y-3">
                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="lastName">
                                            Nom
                                        </label>
                                        <input
                                            id="lastName"
                                            type="text"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="Guillotin"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, lastName: event.target.value })}
                                        />
                                    </div>
                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="firstName">
                                            Prénom
                                        </label>
                                        <input
                                            id="firstName"
                                            type="text"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="Lucas"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, firstName: event.target.value })}
                                        />
                                    </div>
                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="phone">
                                            N° de téléphone
                                        </label>
                                        <input
                                            id="phone"
                                            type="phone"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            style={{ transition: 'all .15s ease' }}
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, phone: event.target.value })}
                                        />
                                    </div>
                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="email">
                                            Adresse email
                                        </label>
                                        <input
                                            id="email"
                                            type="email"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="test@latest.com"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, email: event.target.value })}
                                        />
                                    </div>

                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2">Veux-tu que ton profil soit visible par les autres joueurs ?</label>
                                        <Switch
                                            checked={user.isPublic}
                                            onChange={(checked: boolean) => setUser({ ...user, isPublic: checked })}
                                            className={`${user.isPublic ? 'bg-primary' : 'bg-gray-200'}
                                        elative inline-flex items-center h-6 rounded-full w-11`}
                                        >
                                            <span
                                                aria-hidden="true"
                                                className={`${user.isPublic ? 'translate-x-6' : 'translate-x-1'}
                                            inline-block w-4 h-4 transform bg-white rounded-full`}
                                            />
                                        </Switch>
                                    </div>

                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="password">
                                            Mot de passe
                                        </label>
                                        <input
                                            id="password"
                                            type="password"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="Password"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, password: event.target.value })}
                                        />
                                    </div>

                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="grid-password">
                                            Confirmation du mot de passe
                                        </label>
                                        <input
                                            id="grid-password"
                                            type="password"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="Password"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => checkPasswordConfirmation(event.target.value)}
                                        />
                                        {!passwordEquals && user.password.length > 0 && <small className="text-red-500">Les mots de passes ne correspondent pas !</small>}
                                    </div>

                                    <div className="flex justify-center">
                                        <div className="flex flex-row justify-center items-center gap-x-8">
                                            <label className="block uppercase text-xs font-bold mb-2 ">Je suis joueur</label>
                                            <Switch checked={!isPlayer(user)} onChange={onChangeRole} className={`bg-primary relative inline-flex items-center h-6 rounded-full w-11`}>
                                                <span
                                                    aria-hidden="true"
                                                    className={`${!isPlayer(user) ? 'translate-x-6' : 'translate-x-1'}
                                                inline-block w-4 h-4 transform bg-white rounded-full`}
                                                />
                                            </Switch>
                                            <label className="block uppercase text-xs font-bold mb-2">Je suis gérant</label>
                                        </div>
                                    </div>

                                    <div className="text-center mt-6">
                                        <button
                                            className="bg-primary text-white text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                            type="button"
                                            style={{ transition: 'all .15s ease' }}
                                            onClick={onSubmit}
                                        >
                                            Valider
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="flex flex-wrap mt-6">
                            <div className="w-1/2">
                                <Link to="/login" className="text-primary">
                                    Déjà inscrit ?
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
