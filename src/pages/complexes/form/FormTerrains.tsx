import SelectHours from '../../../components/SelectHours';
import { TrashIcon, ChevronDownIcon, ChevronRightIcon } from '@heroicons/react/outline';
import { Disclosure, Switch } from '@headlessui/react';
import { ComplexRegister } from '../../../models/complex.model';
import { LandRegister } from '../../../models/land.model';
import Sport from '../../../models/sport.model';

type Props = {
    complex: ComplexRegister;
    addLand: () => void;
    deleteLand: (id: number) => void;
    onChangeLand: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>, id: number, arg: string) => void;
    toggleIsInside: (index: number) => void;
    toggleHasEquipement: (index: number) => void;
    onChangeSchedule: (e: React.ChangeEvent<HTMLSelectElement>, land_id: number, day: string, creneau: string) => void;
    formIsValid: boolean;
    sports: Array<Sport>;
};

export default function FormTerrains({ complex, addLand, onChangeLand, deleteLand, toggleIsInside, toggleHasEquipement, onChangeSchedule, formIsValid, sports }: Props): JSX.Element {
    return (
        <>
            <div className="pt-8">
                <div className="sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900">Gestion des terrains</h1>
                    </div>
                </div>
                <div className="my-4">
                    <div
                        onClick={addLand}
                        className="cursor-pointer inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                    >
                        Ajouter un terrain
                    </div>
                </div>
                <div className="flex flex-col">
                    <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                            <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                <table className="min-w-full divide-y divide-gray-300">
                                    <thead className="bg-gray-50">
                                        <tr>
                                            <th scope="col" className="py-3.5 pl-4 pr-3 text-center text-sm font-semibold text-gray-900 sm:pl-6"></th>
                                            <th scope="col" className="py-3.5 pl-4 pr-3 text-center text-sm font-semibold text-gray-900 sm:pl-6">
                                                Sport
                                            </th>
                                            <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                Nom du terrain
                                            </th>
                                            <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                Prix de la réservation
                                            </th>
                                            <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                Est à l&apos;intérieur
                                            </th>
                                            <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                Est équipé
                                            </th>
                                            <th scope="col" className="relative py-3.5 pl-3 pr-4 sm:pr-6"></th>
                                        </tr>
                                    </thead>
                                    <tbody className="divide-y divide-gray-200 bg-white">
                                        {complex.lands.map((land: LandRegister, index: number) => (
                                            <Disclosure key={land.key}>
                                                {({ open }) => (
                                                    <>
                                                        <tr>
                                                            <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                                                <Disclosure.Button className="py-2">{open ? <ChevronDownIcon className="h-6 w-6" /> : <ChevronRightIcon className="h-6 w-6" />}</Disclosure.Button>
                                                            </td>
                                                            <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                                                <select
                                                                    id="sport"
                                                                    value={land.sport}
                                                                    onChange={(e: React.ChangeEvent<HTMLSelectElement>) => onChangeLand(e, land.key, 'sport')}
                                                                    className={
                                                                        (land.sport !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                                                        ' mt-1 block w-full pl-3 pr-10 py-2 text-base focus:outline-none sm:text-sm rounded-md'
                                                                    }
                                                                    defaultValue=""
                                                                >
                                                                    <option value={undefined}>Choisir un sport</option>
                                                                    {sports.map((sport) => (
                                                                        <option key={sport.id} value={`/api/sports/${sport.id}`}>
                                                                            {sport.name}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            </td>
                                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                                                <input
                                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeLand(e, land.key, 'name')}
                                                                    type="text"
                                                                    value={land.name}
                                                                    id={`land_name_${land.key}`}
                                                                    className={
                                                                        (land.name !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                                                        ' shadow-sm block w-full sm:text-sm rounded-md'
                                                                    }
                                                                />
                                                            </td>
                                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                                                <input
                                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeLand(e, land.key, 'price')}
                                                                    type="number"
                                                                    value={land.price}
                                                                    id={`price_${land.key}`}
                                                                    className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                                                                />
                                                            </td>
                                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                                                <Switch.Group as="div" className="flex items-center justify-center h-full">
                                                                    <Switch
                                                                        checked={land.isInside}
                                                                        onChange={() => toggleIsInside(index)}
                                                                        className={
                                                                            (land.isInside ? 'bg-indigo-600 ' : 'bg-gray-200 ') +
                                                                            ' relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-transparent'
                                                                        }
                                                                    >
                                                                        <span
                                                                            aria-hidden="true"
                                                                            className={
                                                                                (land.isInside ? 'translate-x-5 ' : 'translate-x-0 ') +
                                                                                ' pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                                                            }
                                                                        />
                                                                    </Switch>
                                                                </Switch.Group>
                                                            </td>
                                                            <td className="whitespace-nowrap px-3 py-4 text-sm text-gray-500">
                                                                <Switch.Group as="div" className="flex items-center justify-center h-full">
                                                                    <Switch
                                                                        checked={land.hasEquipments}
                                                                        onChange={() => toggleHasEquipement(index)}
                                                                        className={
                                                                            (land.hasEquipments ? 'bg-indigo-600 ' : 'bg-gray-200 ') +
                                                                            ' relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-transparent'
                                                                        }
                                                                    >
                                                                        <span
                                                                            aria-hidden="true"
                                                                            className={
                                                                                (land.hasEquipments ? 'translate-x-5 ' : 'translate-x-0 ') +
                                                                                ' pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'
                                                                            }
                                                                        />
                                                                    </Switch>
                                                                </Switch.Group>
                                                            </td>
                                                            <td className="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                                                                <TrashIcon onClick={() => deleteLand(land.key)} className="h-6 w-6 text-red-500 cursor-pointer" />
                                                            </td>
                                                        </tr>

                                                        <Disclosure.Panel as="tr" className="text-gray-500" key={land.key}>
                                                            <td colSpan={7} className="odd:bg-gray-100">
                                                                <div className="grid grid-cols-5 gap-6 mr-8">
                                                                    <div className="my-2 text-sm text-center"></div>
                                                                    <div className="my-2 text-sm text-center">Ouverture matin</div>
                                                                    <div className="my-2 text-sm text-center">Fermeture matin</div>
                                                                    <div className="my-2 text-sm text-center">Ouverture soir</div>
                                                                    <div className="my-2 text-sm text-center">Fermeture soir</div>
                                                                </div>
                                                                {land.landSchedules.map((schedule) => {
                                                                    return (
                                                                        <div className="grid grid-cols-5 gap-6 mr-8" key={land.key + schedule.day}>
                                                                            <div className="my-2 text-center">{schedule.day}</div>
                                                                            <div className="my-2">
                                                                                <SelectHours onChangeSchedule={(e: React.ChangeEvent<HTMLSelectElement>) => onChangeSchedule(e, land.key, schedule.day, 'startingTimeMorning')} />
                                                                            </div>
                                                                            <div className="my-2">
                                                                                <SelectHours onChangeSchedule={(e: React.ChangeEvent<HTMLSelectElement>) => onChangeSchedule(e, land.key, schedule.day, 'endingTimeMorning')} />
                                                                            </div>
                                                                            <div className="my-2">
                                                                                <SelectHours onChangeSchedule={(e: React.ChangeEvent<HTMLSelectElement>) => onChangeSchedule(e, land.key, schedule.day, 'startingTimeAfternoon')} />
                                                                            </div>
                                                                            <div className="my-2">
                                                                                <SelectHours onChangeSchedule={(e: React.ChangeEvent<HTMLSelectElement>) => onChangeSchedule(e, land.key, schedule.day, 'endingTimeAfternoon')} />
                                                                            </div>
                                                                        </div>
                                                                    );
                                                                })}
                                                            </td>
                                                        </Disclosure.Panel>
                                                    </>
                                                )}
                                            </Disclosure>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
