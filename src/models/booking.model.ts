import Land from './land.model';
import User from './user.model';

export default interface Booking {
    id: number;
    bookedTime: Date;
    participants: User[];
    land: Land | null;
    isCancelled: boolean;
}

export interface BookingRegister {
    bookedTime: string;
    participants: [];
    land: string | undefined;
    isCanceled: false;
}
