export default interface LandSchedule {
    id: number;
    land_id: number;
    startingTimeMorning: number;
    endingTimeMorning: number;
    startingTimeAfternoon: number;
    endingTimeAfternoon: number;
    day: string;
}

export interface LandScheduleRegister {
    startingTimeMorning: number;
    endingTimeMorning: number;
    startingTimeAfternoon: number;
    endingTimeAfternoon: number;
    day: string;
}
