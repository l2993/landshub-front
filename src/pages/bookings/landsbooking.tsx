import 'react-calendar/dist/Calendar.css';

import { AuthContext, AuthContextType } from '../../contexts/auth.context';
import Booking, { BookingRegister } from '../../models/booking.model';
import { BookingContext, BookingContextType } from '../../contexts/booking.context';
import { LandContext, LandContextType } from '../../contexts/land.context';
import { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import Calendar from 'react-calendar';
import CalendarComponent from '../../components/Calendar';
import LandSchedule from '../../models/landSchedule.model';
import LoaderFetching from '../../components/LoaderFetching';
import Toastr from '../../components/Toastr';

export default function LandsBooking(): JSX.Element {
    const navigate = useNavigate();
    const params = useParams();

    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });
    const [date, setDate] = useState<Date>(new Date());
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [bookings, setBookings] = useState<Booking[]>([]);
    const [landSchedules, setLandSchedules] = useState<LandSchedule[]>([]);
    const [selectedBookings, setSelectedBookings] = useState<number[]>([]);
    const [displayBookings, setDisplayBookings] = useState<JSX.Element[]>([]);

    const { getBookingsOfALandAndOfADay, createBooking } = useContext(BookingContext) as BookingContextType;
    const { getLandSchedules } = useContext(LandContext) as LandContextType;
    const { token } = useContext(AuthContext) as AuthContextType;

    const modifyBookings = (selectedBooking: number): void => {
        if (selectedBookings.includes(selectedBooking)) {
            const index = selectedBookings.indexOf(selectedBooking, 0);
            if (index > -1) {
                setSelectedBookings(selectedBookings.splice(index, 1));
            }
        } else {
            selectedBookings.push(selectedBooking);
        }
        setSelectedBookings(selectedBookings.sort((a: number, b: number) => a - b));
        displaySelectedBookings();
    };

    const [calendarComponentProps, setCalendarComponentProps] = useState({
        bookings,
        startingTimeMorning: 6,
        endingTimeMorning: 12,
        startingTimeAfternoon: 13,
        endingTimeAfternoon: 19,
        book: modifyBookings,
    });

    const fetchLandBookings = async (dateSelected: Date): Promise<void> => {
        if (params.landId) {
            const l = await getBookingsOfALandAndOfADay(1, dateSelected, params.landId);
            setBookings(l?.bookings ?? []);
            setIsLoading(false);
        }
    };

    const fetchLandSchedules = async (): Promise<void> => {
        if (params.landId) {
            const l = await getLandSchedules(params.landId);
            setLandSchedules(l ?? []);
            setIsLoading(false);
        }
    };

    const selectTheRightSchedule = (dateSelected: Date): void => {
        const days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

        setIsLoading(true);

        if (landSchedules) {
            const land = landSchedules.find((landSchedule: LandSchedule) => {
                return days[dateSelected.getDay()] === landSchedule.day;
            });

            setCalendarComponentProps({
                ...calendarComponentProps,
                bookings,
                startingTimeMorning: land?.startingTimeMorning ?? 6,
                endingTimeMorning: land?.endingTimeMorning ?? 12,
                startingTimeAfternoon: land?.startingTimeAfternoon ?? 13,
                endingTimeAfternoon: land?.endingTimeAfternoon ?? 19,
            });
            setIsLoading(false);
        }
    };

    const newDateSelected = (dateSelected: Date): void => {
        setDate(dateSelected);
        setIsLoading(true);

        setDisplayBookings([]);
        setSelectedBookings([]);

        selectTheRightSchedule(dateSelected);
        fetchLandBookings(dateSelected);
    };

    useEffect(() => {
        setIsLoading(true);
        if (params.landId && token) {
            fetchLandSchedules();
            fetchLandBookings(date);
        } else {
            navigate('/login');
        }
    }, []);

    useEffect(() => {
        if (landSchedules.length) {
            selectTheRightSchedule(date);
        }
    }, [landSchedules, bookings]);

    const displaySelectedBookings = (): void => {
        const toDisplay: JSX.Element[] = [];
        selectedBookings.forEach((booking: number) => {
            toDisplay.push(
                <p className="block uppercase text-xs font-bold mb-2">
                    - De {booking}H à {booking + 1}H
                </p>,
            );
        });

        setDisplayBookings(toDisplay);
    };

    const onSubmit = async () => {
        if (params.landId !== null) {
            setIsLoading(true);
            selectedBookings.forEach(async (selectedBooking: number) => {
                const booking: BookingRegister = {
                    bookedTime: date.getFullYear() + '-' + (date.getMonth() + 1).toString() + '-' + date.getDate() + 'T' + selectedBooking + ':00:00',
                    participants: [],
                    land: '/api/lands/' + params.landId,
                    isCanceled: false,
                };
                const l = await createBooking(booking);
                if (!l?.id) {
                    setToastr({ type: 'info', title: 'Limite de créneaux atteinte ', message: 'Vous avez déjà sélectionner 4 créneaux' });
                    setShowToastr(true);
                    setTimeout(() => setShowToastr(false), 3000);
                }
            });

            fetchLandBookings(date);

            setDisplayBookings([]);
            setSelectedBookings([]);

            setIsLoading(false);
        }
    };

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            {isLoading ? (
                <LoaderFetching text={'Récupération des réservations du jour sélectionné'} />
            ) : (
                <>
                    <div className="flex h-full flex-col">
                        <div className="flex flex-col md:flex-row flex-auto overflow-hidden bg-white">
                            <div className="flex flex-col w-full gap-y-6 md:w-1/2 max-w-md border-l border-gray-100 py-10 px-8 block">
                                <Calendar onChange={(dateSelected: Date) => newDateSelected(dateSelected)} value={date} />
                                <div className="flex items-center justify-center bg-gray-200 border border-gray-200 rounded-xl shadow-lg">
                                    <div className="w-full p-4">
                                        <div className="text-center mb-3">
                                            <h2 className="text-primary text-2xl font-bold">Réservation</h2>
                                        </div>
                                        <div className="flex flex-col space-y-3">
                                            <div className="relative w-full">
                                                <p className="block uppercase text-xs font-bold mb-2">Pour le {date.toLocaleDateString()}</p>
                                            </div>
                                            <div className="relative w-full">{displayBookings}</div>
                                            <div className="text-center mt-6">
                                                <button
                                                    disabled={!displayBookings.length}
                                                    className={
                                                        (!displayBookings.length ? 'bg-gray-400' : 'bg-primary') +
                                                        ' text-white text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full'
                                                    }
                                                    type="button"
                                                    style={{ transition: 'all .15s ease' }}
                                                    onClick={onSubmit}
                                                >
                                                    Réserver
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex flex-auto flex-col">
                                <CalendarComponent {...calendarComponentProps}></CalendarComponent>
                            </div>
                        </div>
                    </div>
                </>
            )}
        </>
    );
}
