import { ComplexContext, ComplexContextType } from '../../../contexts/complex.context';
import React, { useContext, useEffect, useState } from 'react';
import { SportContext, SportContextType } from '../../../contexts/sport.context';
import { useNavigate, useParams } from 'react-router-dom';

import { ComplexPictureRegister } from '../../../models/complex_picture.model';
import { ComplexRegister } from '../../../models/complex.model';
import FormPhotos from './FormPhotos';
import FormSalle from './FormSalle';
import FormTerrains from './FormTerrains';
import { LandRegister } from '../../../models/land.model';
import { LandScheduleRegister } from '../../../models/landSchedule.model';
import Sport from '../../../models/sport.model';
import Toastr from '../../../components/Toastr';
import { getUserIDAuth } from '../../../functions/Authentication';
import lodash from 'lodash';

const landModel: LandRegister = {
    hasEquipments: false,
    isInside: false,
    key: 0,
    landSchedules: [],
    name: '',
    price: 0,
    sport: '',
};

const days = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];

const landScheduleModel: LandScheduleRegister = {
    day: '',
    endingTimeAfternoon: 0,
    endingTimeMorning: 0,
    startingTimeAfternoon: 0,
    startingTimeMorning: 0,
};

const complexPictureModel: ComplexPictureRegister = {
    key: 0,
    url: '',
};

export default function Form(): JSX.Element {
    const { createComplex, getComplexEdit, editComplex, deleteComplex } = useContext(ComplexContext) as ComplexContextType;
    const { getSports } = useContext(SportContext) as SportContextType;

    const navigate = useNavigate();

    const [complex, setComplex] = useState<ComplexRegister>({
        address: '',
        city: '',
        complexPictures: [
            {
                key: 0,
                url: '',
            },
        ],
        description: '',
        hasChangingRoom: false,
        lands: [
            {
                hasEquipments: false,
                isInside: false,
                key: 0,
                landSchedules: [],
                name: '',
                price: 0,
                sport: '',
            },
        ],
        latitude: '',
        logo: '',
        longitude: '',
        name: '',
        postal: '',
    });

    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [sports, setSports] = useState<Array<Sport>>([]);
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });

    const params = useParams();

    useEffect(() => {
        //si je suis en edit
        if (params.id) {
            setIsEdit(true);
            getEditComplex(params.id);
        }
    }, [params]);

    const [isValidLatitude, setIsValidLatitude] = useState<boolean>(true);
    const [isValidLongitude, setIsValidLongitude] = useState<boolean>(true);
    const [formIsValid, setFormIsValid] = useState<boolean>(true);

    useEffect(() => {
        async function fetchSports(): Promise<void> {
            const tps_sports = await getSports();
            setSports(tps_sports ?? []);
        }
        fetchSports();

        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        lodash.forEach(days, (day) => {
            const new_schedule = lodash.cloneDeep(landScheduleModel);
            new_schedule.day = day;
            tps_complex.lands[0].landSchedules.push(new_schedule);
        });
        setComplex(tps_complex);
    }, []);

    const getEditComplex = async (id: string) => {
        const complex = await getComplexEdit(id);
        lodash.forEach(complex.lands, (land, i) => {
            delete land.id;
            land.key = i;
            const tps_land: { id: number; name: string } = JSON.parse(JSON.stringify(land.sport));
            land.sport = '/api/sports/' + tps_land.id;
        });
        lodash.forEach(complex.complexPictures, (picture, i) => {
            picture.key = i;
        });
        setComplex(complex);
    };

    const addLand = (): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        const new_land: LandRegister = lodash.cloneDeep(landModel);

        if (tps_complex.lands.length > 0) {
            const find: LandRegister = tps_complex.lands[tps_complex.lands.length - 1];
            const last_key: number = find.key + 1;
            new_land.key = last_key;
        }
        lodash.forEach(days, (day) => {
            const new_schedule: LandScheduleRegister = lodash.cloneDeep(landScheduleModel);
            new_schedule.day = day;
            new_land.landSchedules.push(new_schedule);
        });
        tps_complex.lands.push(new_land);
        setComplex(tps_complex);
    };

    const deleteLand = (id: number): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        lodash.remove(tps_complex.lands, (land) => {
            return land.key === id;
        });
        setComplex(tps_complex);
    };

    const addPicture = (): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        const new_picture: ComplexPictureRegister = lodash.cloneDeep(complexPictureModel);

        if (tps_complex.complexPictures.length > 0) {
            const find: ComplexPictureRegister = tps_complex.complexPictures[tps_complex.complexPictures.length - 1];
            const last_key: number = find.key + 1;
            new_picture.key = last_key;
        }
        tps_complex.complexPictures.push(new_picture);
        setComplex(tps_complex);
    };

    const deletePicture = (id: number): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        lodash.remove(tps_complex.complexPictures, (picture) => {
            return picture.key === id;
        });
        setComplex(tps_complex);
    };

    const toggleChangingRoom = (): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        tps_complex.hasChangingRoom = !tps_complex.hasChangingRoom;
        setComplex(tps_complex);
    };

    const toggleIsInside = (index: number): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        tps_complex.lands[index].isInside = !tps_complex.lands[index].isInside;
        setComplex(tps_complex);
    };

    const toggleHasEquipement = (index: number): void => {
        const tps_complex: ComplexRegister = lodash.cloneDeep(complex);
        tps_complex.lands[index].hasEquipments = !tps_complex.lands[index].hasEquipments;
        setComplex(tps_complex);
    };

    const onChangeLand = (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>, id: number, arg: string): void => {
        const tps_complex = lodash.cloneDeep(complex);
        let land = lodash.find(tps_complex.lands, (land) => land.key === id);
        const index = lodash.findIndex(tps_complex.lands, (land) => land.key === id);

        if (land) {
            if (arg == 'price') {
                land = {
                    ...land,
                    [arg]: Number(e.target.value),
                };
            } else {
                land = {
                    ...land,
                    [arg]: e.target.value,
                };
            }
            tps_complex.lands[index] = land;
            setComplex(tps_complex);
        }
    };

    const onChangePicture = (e: React.ChangeEvent<HTMLInputElement>, id: number): void => {
        const tps_complex = lodash.cloneDeep(complex);
        const picture = lodash.find(tps_complex.complexPictures, (picture) => picture.key === id);
        const index = lodash.findIndex(tps_complex.complexPictures, (picture) => picture.key === id);

        if (picture) {
            picture.url = e.target.value;
            tps_complex.complexPictures[index] = picture;
            setComplex(tps_complex);
        }
    };

    const onChangeSchedule = (e: React.ChangeEvent<HTMLSelectElement>, land_id: number, day: string, creneau: string): void => {
        const tps_complex = lodash.cloneDeep(complex);
        const land = lodash.find(tps_complex.lands, (land) => land.key === land_id);
        const index = lodash.findIndex(tps_complex.lands, (land) => land.key === land_id);

        if (land) {
            let find_schedule = lodash.find(land.landSchedules, (schedule) => schedule.day === day);
            const find_schedule_index = lodash.findIndex(land.landSchedules, (schedule) => schedule.day === day);
            if (find_schedule) {
                find_schedule = {
                    ...find_schedule,
                    [creneau]: Number(e.target.value),
                };
                land.landSchedules[find_schedule_index] = find_schedule;
            }
            tps_complex.lands[index] = land;
            setComplex(tps_complex);
        }
    };

    const saveComplex = async (): Promise<void> => {
        if (checkFormValid()) {
            const tps_complex = lodash.cloneDeep(complex);
            tps_complex.latitude = Number(tps_complex.latitude);
            tps_complex.longitude = Number(tps_complex.longitude);
            const res = await createComplex(tps_complex);
            if (res.id) {
                setToastr({ type: 'success', title: 'Création réussie', message: 'Votre complexe a bien été créé.' });
                setShowToastr(true);
                setTimeout(() => {
                    setToastr({ type: '', title: '', message: '' });
                    setShowToastr(false);
                    navigate('/complexes');
                }, 5000);
            }
        } else {
            window.scrollTo({ behavior: 'smooth', top: 0 });
            setFormIsValid(false);
        }
    };

    const updateComplex = async (): Promise<void> => {
        if (checkFormValid()) {
            const tps_complex = lodash.cloneDeep(complex);
            tps_complex.latitude = Number(tps_complex.latitude);
            tps_complex.longitude = Number(tps_complex.longitude);
            const res = await editComplex(tps_complex);
            if (res.id) {
                setToastr({ type: 'success', title: 'Modification réussie', message: 'Votre compelxe a bien été modifié.' });
                setShowToastr(true);
                setTimeout(() => {
                    setToastr({ type: '', title: '', message: '' });
                    setShowToastr(false);
                }, 5000);
            }
        } else {
            window.scrollTo({ behavior: 'smooth', top: 0 });
            setFormIsValid(false);
        }
    };

    const isValideCoord = (coord: string, arg: string): string => {
        const regEx = /^([-]?[\d]{0,3})[.]?[\d]{0,9}?$/g;

        !coord.match(regEx) ? (arg === 'latitude' ? setIsValidLatitude(false) : setIsValidLongitude(false)) : arg === 'latitude' ? setIsValidLatitude(true) : setIsValidLongitude(true);

        return coord;
    };

    const checkFormValid = (): boolean => {
        let res = true;
        //Parcourir les keys
        if (complex.name === '' || complex.description === '' || complex.logo === '' || complex.address === '' || complex.postal === '' || complex.city === '') {
            res = false;
        }

        lodash.forEach(complex.complexPictures, (picture) => {
            if (picture.url === '') {
                res = false;
            }
        });

        lodash.forEach(complex.lands, (land) => {
            if (land.sport === '' || land.name === '') {
                res = false;
            }
        });

        return res;
    };

    const canDelete = (): boolean => {
        let res = false;
        lodash.forEach(complex.users, (user) => {
            if (user.id === getUserIDAuth()) {
                res = true;
            }
        });

        return res;
    };

    const deletedComplex = async (id: string | undefined): Promise<void> => {
        const res = await deleteComplex(id);
        if (res) {
            setToastr({ type: 'success', title: 'Suppression réussie', message: 'Votre salle à bien été supprimée.' });
            setShowToastr(true);
            setTimeout(() => {
                setToastr({ type: '', title: '', message: '' });
                setShowToastr(false);
                navigate('/complexes');
            }, 4000);
        } else {
            setToastr({ type: 'error', title: 'Echec de la suppression', message: 'Un problème est survenu lors de la suppression.' });
            setShowToastr(true);
            setTimeout(() => {
                setToastr({ type: '', title: '', message: '' });
                setShowToastr(false);
            }, 4000);
        }
    };

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            <div className="mx-40 my-8 bg-white rounded-md p-8">
                <form className="space-y-8 divide-y divide-gray-200">
                    <div className="space-y-8 divide-y divide-gray-200">
                        <FormSalle
                            formIsValid={formIsValid}
                            isValidLatitude={isValidLatitude}
                            isValidLongitude={isValidLongitude}
                            complex={complex}
                            setComplex={setComplex}
                            isValideCoord={isValideCoord}
                            toggleChangingRoom={toggleChangingRoom}
                            isEdit={isEdit}
                        />

                        <FormTerrains
                            formIsValid={formIsValid}
                            complex={complex}
                            addLand={addLand}
                            deleteLand={deleteLand}
                            onChangeLand={onChangeLand}
                            toggleIsInside={toggleIsInside}
                            toggleHasEquipement={toggleHasEquipement}
                            onChangeSchedule={onChangeSchedule}
                            sports={sports}
                        />

                        <FormPhotos formIsValid={formIsValid} complex={complex} addPicture={addPicture} deletePicture={deletePicture} onChangePicture={onChangePicture} />
                    </div>

                    {isEdit ? (
                        <>
                            <div className="pt-5">
                                <div className="flex justify-start">
                                    <div
                                        onClick={updateComplex}
                                        className="cursor-pointer ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                    >
                                        Modifier
                                    </div>
                                    {canDelete() ? (
                                        <div
                                            onClick={() => deletedComplex(params.id)}
                                            className="cursor-pointer ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
                                        >
                                            Supprimer
                                        </div>
                                    ) : null}
                                </div>
                            </div>
                        </>
                    ) : (
                        <div className="pt-5">
                            <div className="flex justify-start">
                                <div
                                    onClick={saveComplex}
                                    className="cursor-pointer ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                >
                                    Enregistrer
                                </div>
                            </div>
                        </div>
                    )}
                </form>
            </div>
        </>
    );
}
