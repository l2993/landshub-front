import { useContext, useEffect, useState } from 'react';
import { ComplexContext, ComplexContextType } from '../../../contexts/complex.context';
import { SportContext, SportContextType } from '../../../contexts/sport.context';
import Complex from '../../../models/complex.model';
import ComplexCard from '../../../components/cards/ComplexCard';
import Pagination from '../../../components/Pagination';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { Switch } from '@headlessui/react';
import LoaderFetching from '../../../components/LoaderFetching';

// eslint-disable-next-line import/named

// import PopoverTime from '../../components/PopoverTime';

const animatedComponents = makeAnimated();

export default function ComplexList(): JSX.Element {
    const pageSize = 8;
    const { getComplexes } = useContext(ComplexContext) as ComplexContextType;
    const { getSports } = useContext(SportContext) as SportContextType;

    const [complexes, setComplexes] = useState<Complex[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [search, setSearch] = useState<string>('');
    const [sports, setSports] = useState<{ id: number; name: string }[]>([]);
    const [sportsSelected, setSportsSelected] = useState<{ value: number; label: string }[]>([]);
    const [hasChangingRoom, setHasChangingRoom] = useState<boolean>(false);
    // Page actuelle
    const [page, setPage] = useState<number>(1);
    //Indique si la pagination est necessaire ou non par un boolean
    const [paginate, setPaginate] = useState<boolean>(false);
    const [maxPage, setMaxPage] = useState<number>(1);

    useEffect(() => {
        setIsLoading(true);
        async function fetchComplexes(): Promise<void> {
            const c = await getComplexes(page, search, sportsSelected, hasChangingRoom);
            setComplexes(c?.complexes ?? []);
            if (c) setUpPagination(c);
        }

        async function fecthSports(): Promise<void> {
            const s = await getSports();
            setSports(s ?? []);
        }

        fetchComplexes();
        fecthSports();

        setIsLoading(false);
    }, []);

    useEffect(() => {
        setIsLoading(true);
        async function fetchComplexes(): Promise<void> {
            const c = await getComplexes(page, search, sportsSelected, hasChangingRoom);
            setComplexes(c?.complexes ?? []);
            if (c) setUpPagination(c);
        }
        fetchComplexes();
        setIsLoading(false);
    }, [sportsSelected, search, hasChangingRoom, page]);

    const handleChangeSports = (newValue: unknown): void => {
        setSportsSelected(newValue as { value: number; label: string }[]);
    };

    const setUpPagination = (c: { complexes: Complex[]; totalCount: number }): void => {
        if (c?.complexes.length < c?.totalCount) {
            setPaginate(true);
            setMaxPage(Math.ceil(c?.totalCount / pageSize));
        } else {
            setPaginate(false);
        }
    };

    //Fonction pour passer à la page suivante
    function next(): void {
        setPage((page) => Math.min(page + 1, maxPage));
    }

    //Fonction pour passer à la page précédente
    function prev(): void {
        setPage((page) => Math.max(page - 1, 1));
    }

    //Fonction pour passer à une page indiqué
    function jump(pageNb: number): void {
        const pageNumber = Math.max(1, pageNb);
        setPage(() => Math.min(pageNumber, maxPage));
    }

    return (
        <>
            {isLoading ? (
                <LoaderFetching text={'Chargement des complexes en cours ...'} />
            ) : (
                <div className="border border-gray-200 flex flex-col gap-8 m-5 p-5 bg-white rounded-md shadow-2xl">
                    <div className="flex gap-6 flex-col md:flex-row md:items-center">
                        <input
                            type="search"
                            id="search"
                            placeholder="Rechercher"
                            className="border border-gray-300 bg-gray-200 p-2 rounded-md shadow-xl"
                            onBlur={(event: React.ChangeEvent<HTMLInputElement>) => setSearch(event.target.value)}
                        />
                        <div>
                            <Select
                                placeholder="Choisir un sport"
                                closeMenuOnSelect={false}
                                components={animatedComponents}
                                isMulti
                                options={sports.map((sport: { id: number; name: string }) => {
                                    return { value: sport.id, label: sport.name };
                                })}
                                onChange={handleChangeSports}
                            />
                        </div>
                        <div className="flex gap-x-2">
                            <span>Vestiaires : </span>
                            <Switch
                                checked={hasChangingRoom}
                                onChange={(checked: boolean) => [setHasChangingRoom(checked)]}
                                className={`${hasChangingRoom ? 'bg-primary' : 'bg-gray-200'}
                                        elative inline-flex items-center h-6 rounded-full w-11`}
                            >
                                <span
                                    aria-hidden="true"
                                    className={`${hasChangingRoom ? 'translate-x-6' : 'translate-x-1'}
                                            inline-block w-4 h-4 transform bg-white rounded-full`}
                                />
                            </Switch>
                        </div>
                    </div>
                    <div className="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4">
                        {complexes.map((complex: Complex) => {
                            return <ComplexCard key={complex.id} complex={complex} />;
                        })}
                    </div>
                    <Pagination currentPage={page} next={next} prev={prev} jump={jump} paginate={paginate} maxPage={maxPage} />
                </div>
            )}
        </>
    );
}
