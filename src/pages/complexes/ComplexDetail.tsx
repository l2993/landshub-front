import 'react-responsive-carousel/lib/styles/carousel.min.css';

import { ChevronDownIcon, ChevronRightIcon, StarIcon as StarIconOutline } from '@heroicons/react/outline';
import { ComplexContext, ComplexContextType } from '../../contexts/complex.context';
import { Link, useParams } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';

import Badge from '../../components/Badge';
import { Carousel } from 'react-responsive-carousel';
import Complex from '../../models/complex.model';
import ComplexPicture from '../../models/complex_picture.model';
import { Disclosure } from '@headlessui/react';
import Land from '../../models/land.model';
import { isAuthenticated, isPlayer } from '../../functions/Authentication';
import LoaderFetching from '../../components/LoaderFetching';
import { StarIcon as StarIconSolid } from '@heroicons/react/solid';
import User from '../../models/user.model';

export default function ComplexDetail(): JSX.Element {
    // Variables de contenu
    const [complex, setComplex] = useState<Complex>();
    const [complexPictures, setComplexPictures] = useState<ComplexPicture[]>([]);
    const [complexUsers, setComplexUsers] = useState<User[]>([]);
    const [complexLands, setComplexLands] = useState<Land[]>([]);

    const [isLoading, setIsLoading] = useState<boolean>(false);
    const params = useParams();
    const { getComplex } = useContext(ComplexContext) as ComplexContextType;

    useEffect(() => {
        setIsLoading(true);
        async function fetchComplex(): Promise<void> {
            if (params.complexId) {
                const c = await getComplex(params.complexId);
                if (c) {
                    setComplex(c);
                    setComplexPictures(c.complexPictures);
                    setComplexUsers(c.users);
                    setComplexLands(c.lands);
                }
                setIsLoading(false);
            }
        }
        fetchComplex();
    }, []);

    return (
        <>
            {isLoading ? (
                <LoaderFetching text={'Récupération des informations du complexe...'} />
            ) : (
                <div className="mx-8 md:mx-32">
                    <div className="mt-8 bg-gray-50 overflow-hidden shadow rounded-lg">
                        <div className="px-4 py-5 sm:px-6">
                            <p className="font-bold">{complex?.name}</p>
                        </div>
                        <div className="bg-gray-50 px-4 py-5 sm:p-6 flex flex-col md:grid md:grid-cols-2 md:gap-4">
                            <div className="flex flex-col">
                                <Carousel autoPlay={true} showArrows={true} showThumbs={false}>
                                    {complexPictures?.map((complexPicture: ComplexPicture) => (
                                        <div className="h-52" key={complexPicture.id}>
                                            <img className="h-52 object-contain" src={complexPicture?.url !== '' ? complexPicture?.url : '/placeholder.png'} />
                                        </div>
                                    ))}
                                </Carousel>
                                <div className="grid grid-cols-5 gap-1 items-center">
                                    <div className="col-span-3 items-center justify-center">
                                        <div className="flex flex-row items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mt-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                            </svg>
                                            <div className="flex flex-col ml-2">
                                                <div className="pt-2">{complex?.address},</div>
                                                <div>
                                                    {complex?.postal} {complex?.city}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="flex flex-row items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mt-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="2">
                                                <path
                                                    strokeLinecap="round"
                                                    strokeLinejoin="round"
                                                    d="M9 17V7m0 10a2 2 0 01-2 2H5a2 2 0 01-2-2V7a2 2 0 012-2h2a2 2 0 012 2m0 10a2 2 0 002 2h2a2 2 0 002-2M9 7a2 2 0 012-2h2a2 2 0 012 2m0 10V7m0 10a2 2 0 002 2h2a2 2 0 002-2V7a2 2 0 00-2-2h-2a2 2 0 00-2 2"
                                                />
                                            </svg>
                                            <div className="flex flex-col ml-2">
                                                <div className="pt-2">{complex?.hasChangingRoom ? 'Vestiaires inclus' : 'Pas de vestiaires'}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-span-2 items-center justify-center">
                                        {complexUsers?.map((complexUser: User) => (
                                            <Link to={`/profile/${complexUser.id}`} className="flex flex-row mt-1" key={complexUser.id}>
                                                <img className="h-6 object-contain" src={complexUser?.avatarUrl !== '' ? complexUser?.avatarUrl : '/placeholder.png'} />
                                                <p>{complexUser.firstname + ' ' + complexUser.lastname}</p>
                                            </Link>
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div className="flex flex-col mt-4 md:mt-0">
                                <p>{complex?.description ? complex?.description : 'Pas de description pour ce complexe :/'}</p>
                            </div>
                            <div className="relative col-span-3 mt-8">
                                <div className="absolute inset-0 flex items-center" aria-hidden="true">
                                    <div className="w-full border-t border-gray-300" />
                                </div>
                                <div className="relative flex justify-center">
                                    <span className="px-3 text-lg font-medium text-gray-900 bg-gray-50">Terrains disponibles</span>
                                </div>
                            </div>
                            <div className="col-span-3">
                                <div className="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                    <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                                        <div className="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                            <table className="min-w-full divide-y divide-gray-300">
                                                <thead className="bg-gray-50">
                                                    <tr>
                                                        <th scope="col" className="py-3.5 pl-4 pr-3 text-center text-sm font-semibold text-gray-900 sm:pl-6"></th>
                                                        <th scope="col" className="py-3.5 pl-4 pr-3 text-center text-sm font-semibold text-gray-900 sm:pl-6">
                                                            Nom du terrain
                                                        </th>
                                                        <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                            Sport
                                                        </th>
                                                        <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                            Est en intérieur
                                                        </th>
                                                        <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                            Est équipé
                                                        </th>
                                                        <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900">
                                                            Prix
                                                        </th>
                                                        <th scope="col" className="px-3 py-3.5 text-center text-sm font-semibold text-gray-900"></th>
                                                    </tr>
                                                </thead>
                                                <tbody className="divide-y divide-gray-200 bg-white">
                                                    {complexLands?.map((land, index) => (
                                                        <Disclosure key={land.id}>
                                                            {({ open }) => (
                                                                <>
                                                                    <tr>
                                                                        <td className="whitespace-nowrap text-center py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
                                                                            <Disclosure.Button className="py-2">{open ? <ChevronDownIcon className="h-6 w-6" /> : <ChevronRightIcon className="h-6 w-6" />}</Disclosure.Button>
                                                                        </td>
                                                                        <td className="whitespace-nowrap text-center py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{land.name}</td>
                                                                        <td className="whitespace-nowrap text-center px-3 py-4 text-sm text-gray-500">{land.sport.name}</td>
                                                                        <td className="whitespace-nowrap text-center px-3 py-4 text-sm text-gray-500">
                                                                            <Badge textColor={'text-white'} backgroundColor={land.isInside ? 'bg-green-500' : 'bg-red-500'} label={land.isInside ? 'Oui' : 'Non'}></Badge>
                                                                        </td>
                                                                        <td className="whitespace-nowrap text-center px-3 py-4 text-sm text-gray-500">
                                                                            <Badge textColor={'text-white'} backgroundColor={land.hasEquipments ? 'bg-green-500' : 'bg-red-500'} label={land.hasEquipments ? 'Oui' : 'Non'}></Badge>
                                                                        </td>
                                                                        <td className="whitespace-nowrap text-center px-3 py-4 text-sm text-gray-500">{land.price} €/h</td>
                                                                        <td className="whitespace-nowrap text-center px-3 py-4 text-sm text-gray-500">
                                                                            {isAuthenticated() && isPlayer() ? (
                                                                                <Link to={`/landsbooking/${land.id}`} className="col-span-2 mt-2">
                                                                                    <div className="text-center">
                                                                                        <div
                                                                                            className="bg-primary text-white text-sm font-bold p-2 rounded shadow hover:shadow-lg outline-none focus:outline-none"
                                                                                            style={{ transition: 'all .15s ease' }}
                                                                                        >
                                                                                            Réserver
                                                                                        </div>
                                                                                    </div>
                                                                                </Link>
                                                                            ) : null}
                                                                        </td>
                                                                    </tr>

                                                                    <Disclosure.Panel as="tr" className="text-gray-500" key={land.id}>
                                                                        <td colSpan={7} className="odd:bg-gray-100">
                                                                            <div className="grid grid-cols-5 gap-6 mr-8">
                                                                                <div className="my-2 text-sm text-center"></div>
                                                                                <div className="my-2 text-sm text-center">Ouverture matin</div>
                                                                                <div className="my-2 text-sm text-center">Fermeture matin</div>
                                                                                <div className="my-2 text-sm text-center">Ouverture soir</div>
                                                                                <div className="my-2 text-sm text-center">Fermeture soir</div>
                                                                            </div>
                                                                            {land.landSchedules.map((schedule) => {
                                                                                return (
                                                                                    <div className="grid grid-cols-5 gap-6 mr-8" key={land.id + schedule.day}>
                                                                                        <div className="my-2 text-center">{schedule.day}</div>
                                                                                        <div className="my-2 text-center">{schedule.startingTimeMorning}h</div>
                                                                                        <div className="my-2 text-center">{schedule.endingTimeMorning}h</div>
                                                                                        <div className="my-2 text-center">{schedule.startingTimeAfternoon}h</div>
                                                                                        <div className="my-2 text-center">{schedule.endingTimeAfternoon}h</div>
                                                                                    </div>
                                                                                );
                                                                            })}
                                                                        </td>
                                                                    </Disclosure.Panel>
                                                                </>
                                                            )}
                                                        </Disclosure>
                                                    ))}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="relative col-span-3 mt-8">
                                <div className="absolute inset-0 flex items-center" aria-hidden="true">
                                    <div className="w-full border-t border-gray-300" />
                                </div>
                                <div className="relative flex justify-center">
                                    <span className="px-3 text-lg font-medium text-gray-900 bg-gray-50">Avis des utilisateurs</span>
                                </div>
                            </div>
                            <ul role="list" className="col-span-3 divide-y divide-gray-200">
                                {complex?.reviews.map((review) => (
                                    <li key={review.id} className="grid grid-cols-6 px-8 py-4">
                                        <div className="justify-center items-center col-span-1 flex flex-col">
                                            <img className="h-12 object-contain" src={review.reviewer.avatarUrl !== '' ? review.reviewer.avatarUrl : '/placeholder.png'} />
                                            <p className="font-semibold">{review.reviewer.firstname + ' ' + review.reviewer.lastname}</p>
                                        </div>
                                        <div className="col-span-5 flex flex-col ml-4">
                                            <ul className="flex items-center gap-x-1">
                                                {[1, 2, 3, 4, 5].map((index: number) => {
                                                    return (
                                                        <li key={`${complex}_${index}`}>
                                                            {review.rate >= index - 0.5 ? <StarIconSolid className="h-6 w-auto text-yellow-400" /> : <StarIconOutline className="h-6 w-auto text-yellow-400" />}
                                                        </li>
                                                    );
                                                })}
                                            </ul>
                                            <p>{review.message}</p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}
