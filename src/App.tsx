import './App.css';
import './index.css';

import { AuthProvider } from './contexts/auth.context';
import { BookingProvider } from './contexts/booking.context';
import { ComplexProvider } from './contexts/complex.context';
import { LandProvider } from './contexts/land.context';
import Navbar from './components/Navbar';
import { Outlet } from 'react-router-dom';
import { SportProvider } from './contexts/sport.context';
import { UserProvider } from './contexts/user.context';

function App(): JSX.Element {
    return (
        <>
            <AuthProvider>
                <UserProvider>
                    <ComplexProvider>
                        <LandProvider>
                            <BookingProvider>
                                <SportProvider>
                                    <div>
                                        <header>
                                            <Navbar />
                                        </header>
                                        <main>
                                            <Outlet />
                                        </main>
                                    </div>
                                </SportProvider>
                            </BookingProvider>
                        </LandProvider>
                    </ComplexProvider>
                </UserProvider>
            </AuthProvider>
        </>
    );
}

export default App;
