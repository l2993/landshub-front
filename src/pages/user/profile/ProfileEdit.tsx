import { Switch } from '@headlessui/react';
import { useContext, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import LoaderFetching from '../../../components/LoaderFetching';
import Toastr from '../../../components/Toastr';
import { UserContext, UserContextType } from '../../../contexts/user.context';
import { ProfileRegister } from '../../../models/user.model';

export default function ProfileEdit(): JSX.Element {
    const { getProfile, editProfile } = useContext(UserContext) as UserContextType;
    const [user, setUser] = useState<ProfileRegister>({
        firstname: '',
        lastname: '',
        phone: '',
        email: '',
        isPublic: false,
        avatarUrl: '',
    });
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });

    const navigate = useNavigate();

    useEffect(() => {
        setIsLoading(true);
        async function fetchUser() {
            const u = await getProfile();
            if (u) {
                setUser({
                    firstname: u.firstname ?? '',
                    lastname: u.lastname ?? '',
                    phone: u.phone ?? '',
                    email: u.email ?? '',
                    isPublic: u.isPublic,
                    avatarUrl: u.avatarUrl ?? '',
                });
            } else {
                setToastr({ type: 'error', title: 'Erreur inattentue', message: "Le profil n'a pas pu être récupéré" });
                setShowToastr(true);
            }
            setIsLoading(false);
        }
        fetchUser();
    }, []);

    const formIsValid = (): boolean => {
        return false;
    };

    const submitForm = async (): Promise<void> => {
        const newUser = await editProfile(user);
        if (!newUser) {
            setToastr({ type: 'error', title: 'Erreur inattentue', message: "Le profil n'a pas pu être modifié" });
            setShowToastr(true);
            setTimeout(() => navigate('/profile'), 1500);
        } else {
            setToastr({ type: 'success', title: 'Profil modifié avec succès', message: 'Les modifications ont été enregistrées' });
            setShowToastr(true);
        }
    };

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            {isLoading ? (
                <LoaderFetching text={'Chargement des données en cours'} />
            ) : (
                <div className="m-5 bg-white rounded-md p-8">
                    <div className="flex w-full">
                        <h3 className="text-lg leading-6 font-medium text-gray-900">Édition du profil</h3>
                        <div className="r-0">
                            <Link
                                to="/profile"
                                className="cursor-pointer inline-flex items-center justify-center rounded-md border border-transparent bg-primary px-4 py-2 text-sm font-medium text-white shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                            >
                                Retour au profil
                            </Link>
                        </div>
                    </div>
                    <form className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-2">
                        <div>
                            <label htmlFor="lastname" className="block text-sm font-medium text-gray-700">
                                Nom
                            </label>
                            <div className="mt-1">
                                <input
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, lastname: e.target.value })}
                                    value={user?.lastname}
                                    type="text"
                                    id="lastname"
                                    className={
                                        (user?.lastname !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                        ' shadow-sm block w-full sm:text-sm  rounded-md'
                                    }
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="firstname" className="block text-sm font-medium text-gray-700">
                                Prénom
                            </label>
                            <div className="mt-1">
                                <input
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, firstname: e.target.value })}
                                    value={user?.firstname}
                                    type="text"
                                    id="firstname"
                                    className={
                                        (user?.firstname !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                        ' shadow-sm block w-full sm:text-sm  rounded-md'
                                    }
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                Adresse email
                            </label>
                            <div className="mt-1">
                                <input
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, email: e.target.value })}
                                    type="email"
                                    value={user.email}
                                    id="email"
                                    className={
                                        (user.email !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                        ' shadow-sm block w-full sm:text-sm rounded-md'
                                    }
                                />
                            </div>
                        </div>
                        <div>
                            <label htmlFor="avatar" className="block text-sm font-medium text-gray-700">
                                Avatar
                            </label>
                            <div className="mt-1 sm:col-span-2">
                                <div className="max-w-lg flex rounded-md shadow-sm">
                                    <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">URL</span>
                                    <input
                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, avatarUrl: e.target.value })}
                                        type="text"
                                        value={user.avatarUrl}
                                        id="avatar"
                                        className={
                                            (user.avatarUrl !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                            ' flex-1 block w-full min-w-0 rounded-none rounded-r-md sm:text-sm'
                                        }
                                        placeholder="https://"
                                    />
                                </div>
                            </div>
                        </div>
                        <Switch.Group as="div" className="flex items-center h-full">
                            <Switch
                                checked={user.isPublic}
                                onChange={(checked: boolean) => setUser({ ...user, isPublic: checked })}
                                className={
                                    (user.isPublic ? 'bg-indigo-600 ' : 'bg-gray-200 ') +
                                    ' relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-transparent'
                                }
                            >
                                <span
                                    aria-hidden="true"
                                    className={(user.isPublic ? 'translate-x-5 ' : 'translate-x-0 ') + ' pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'}
                                />
                            </Switch>
                            <Switch.Label as="span" className="ml-3">
                                <span className="text-sm font-medium text-gray-900">Profil public</span>
                            </Switch.Label>
                        </Switch.Group>
                        <div>
                            <label htmlFor="phone" className="block text-sm font-medium text-gray-700">
                                N° de téléphone
                            </label>
                            <div className="mt-1 sm:col-span-2">
                                <div className="max-w-lg flex rounded-md shadow-sm">
                                    <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">N°</span>
                                    <input
                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, phone: e.target.value })}
                                        type="text"
                                        value={user.phone}
                                        id="phone"
                                        className={
                                            (user.phone !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                            ' flex-1 block w-full min-w-0 rounded-none rounded-r-md sm:text-sm'
                                        }
                                        placeholder="0556874521"
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="my-4">
                            <div
                                onClick={() => submitForm()}
                                className="cursor-pointer inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                            >
                                Enregistrer
                            </div>
                        </div>
                    </form>
                </div>
            )}
        </>
    );
}
