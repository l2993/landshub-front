import { AuthContext, AuthContextType } from '../contexts/auth.context';
import DecodedToken, { emptyDecodedToken } from '../models/decodedToken.model';
import { Disclosure, Menu, Transition } from '@headlessui/react';
import { Fragment, useContext, useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { MenuIcon, XIcon } from '@heroicons/react/outline';
import { UserCircleIcon } from '@heroicons/react/solid';

import Logo from '../components/Logo';
import Toastr from './Toastr';
import { getProfilPicture } from '../functions/Authentication';
import jwtDecode from 'jwt-decode';

function classNames(...classes: string[]): string {
    return classes.filter(Boolean).join(' ');
}

function Navbar(): JSX.Element {
    const { token, logout } = useContext(AuthContext) as AuthContextType;

    const [navigation, setNavigation] = useState<{ name: string; href: string }[]>([]);
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });
    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        setShowToastr(false);
    }, []);

    useEffect(() => {
        setUpPaginationItems();
    }, [token]);

    const setUpPaginationItems = (): void => {
        const decodedToken: DecodedToken = token === '' ? emptyDecodedToken : jwtDecode(token);
        // items de la navbar
        let navigation: { name: string; href: string }[] = [];
        if (decodedToken.exp === 0) {
            navigation = [
                { name: 'Inscription', href: '/register' },
                { name: 'Les complexes', href: '/complexes' },
            ];
        } else if (decodedToken.roles.includes('ROLE_PLAYER')) {
            navigation = [
                { name: 'Les complexes', href: '/complexes' },
                { name: 'Joueurs', href: '/players' },
            ];
        } else if (decodedToken.roles.includes('ROLE_MANAGER')) {
            navigation = [
                { name: 'Mes complexes', href: '/complexes' },
                { name: 'Créer un complexe', href: '/complexes/new' },
            ];
        }

        setNavigation(navigation);
    };

    const onLogout = (): void => {
        //Supprime le token d'authentification
        logout();

        setToastr({ type: 'success', title: 'Déconnexion réussie', message: 'Vous êtes maintenant déconnecté de LandsHub' });
        setShowToastr(true);
        setTimeout(() => {
            setShowToastr(false);
            navigate('/login');
        }, 1500);
    };

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            <Disclosure as="nav" className="bg-primary">
                {({ open }) => (
                    <>
                        <div className="mx-2 px-2">
                            <div className="relative flex items-center justify-between h-16">
                                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                                    {/* Mobile menu button*/}
                                    <Disclosure.Button className="inline-flex items-center justify-center p-2 rounded-md text-white hover:text-white hover:bg-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                                        <span className="sr-only">Open main menu</span>
                                        {open ? <XIcon className="block h-6 w-6" aria-hidden="true" /> : <MenuIcon className="block h-6 w-6" aria-hidden="true" />}
                                    </Disclosure.Button>
                                </div>
                                <div className="flex-1 flex items-center justify-center sm:justify-start">
                                    <div className="flex-shrink-0 flex items-center">
                                        <Link to="/complexes">
                                            <Logo />
                                        </Link>
                                    </div>
                                    <div className="hidden sm:block sm:ml-6">
                                        <div className="flex space-x-4">
                                            {navigation.map((item) => (
                                                <Link
                                                    key={item.name}
                                                    to={item.href}
                                                    className={classNames(location.pathname === item.href ? 'bg-white text-primary font-bold' : 'text-white font-medium hover:bg-blue-400 hover:font-bold', 'px-3 py-2 rounded-md text-sm')}
                                                    aria-current={location.pathname === item.href ? 'page' : undefined}
                                                >
                                                    {item.name}
                                                </Link>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                                <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                                    {token === '' ? (
                                        <Link to="/login">
                                            <UserCircleIcon className="h-8 w-8 rounded-full bg-white text-primary" />
                                        </Link>
                                    ) : (
                                        <Menu as="div" className="ml-3 relative">
                                            {/* Profile dropdown */}
                                            <div>
                                                <Menu.Button className="bg-white flex text-sm rounded-full focus:outline-none ">
                                                    <span className="sr-only">Open user menu</span>
                                                    <img className="h-9 w-9 rounded-full" src={getProfilPicture()} alt="" />
                                                </Menu.Button>
                                            </div>
                                            <Transition
                                                as={Fragment}
                                                enter="transition ease-out duration-100"
                                                enterFrom="transform opacity-0 scale-95"
                                                enterTo="transform opacity-100 scale-100"
                                                leave="transition ease-in duration-75"
                                                leaveFrom="transform opacity-100 scale-100"
                                                leaveTo="transform opacity-0 scale-95"
                                            >
                                                <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                    <Menu.Item>
                                                        {({ active }) => (
                                                            <Link to="/profile" className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700')}>
                                                                Mon profil
                                                            </Link>
                                                        )}
                                                    </Menu.Item>
                                                    <Menu.Item>
                                                        {({ active }) => (
                                                            <div onClick={() => onLogout()} className={classNames(active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm text-gray-700 cursor-pointer')}>
                                                                Déconnexion
                                                            </div>
                                                        )}
                                                    </Menu.Item>
                                                </Menu.Items>
                                            </Transition>
                                        </Menu>
                                    )}
                                </div>
                            </div>
                        </div>

                        <Disclosure.Panel className="sm:hidden">
                            <div className="px-2 pt-2 pb-3 space-y-1">
                                {navigation.map((item) => (
                                    <Disclosure.Button
                                        key={item.name}
                                        as="a"
                                        href={item.href}
                                        className={classNames(location.pathname === item.href ? 'bg-white text-primary font-bold' : 'text-white font-medium hover:bg-blue-400', 'block px-3 py-2 rounded-md text-base font-medium')}
                                        aria-current={location.pathname === item.href ? 'page' : undefined}
                                    >
                                        {item.name}
                                    </Disclosure.Button>
                                ))}
                            </div>
                        </Disclosure.Panel>
                    </>
                )}
            </Disclosure>
        </>
    );
}

export default function withRouter(): JSX.Element {
    return <Navbar />;
}
