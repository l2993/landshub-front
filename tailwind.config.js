module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {
            colors: {
                primary: '#2688ff',
                primaryHover: '#4f9fff',
            },
        },
    },
    plugins: [require('@tailwindcss/forms')],
};
