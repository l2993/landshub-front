import ComplexPicture, { ComplexPictureRegister } from './complex_picture.model';
import Land, { LandRegister } from './land.model';
import Review from './review.model';
import User from './user.model';

export default interface Complex {
    id: number;
    name: string;
    description: string;
    logo: string;
    address: string;
    postal: string;
    city: string;
    longitude: number;
    latitude: number;
    hasChangingRoom: boolean;
    lands: Array<Land>;
    complexPictures: Array<ComplexPicture>;
    reviews: Array<Review>;
    users: Array<User>;
}

export interface ComplexRegister {
    id?: number;
    name: string;
    description: string;
    logo: string;
    address: string;
    postal: string;
    city: string;
    longitude: number | string;
    latitude: number | string;
    hasChangingRoom: boolean;
    lands: Array<LandRegister>;
    complexPictures: Array<ComplexPictureRegister>;
    users?: Array<User>;
}
