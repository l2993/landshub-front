import LandSchedule, { LandScheduleRegister } from './landSchedule.model';

import Sport from './sport.model';

export default interface Land {
    id: number;
    complex_id: number;
    name: string;
    price: number;
    isInside: boolean;
    hasEquipments: boolean;
    sport: Sport;
    landSchedules: Array<LandSchedule>;
}

export interface LandRegister {
    id?: number;
    key: number;
    sport: string;
    complex_id?: number;
    name: string;
    price: number;
    isInside: boolean;
    hasEquipments: boolean;
    landSchedules: Array<LandScheduleRegister>;
}
