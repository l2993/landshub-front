import { TrashIcon } from '@heroicons/react/outline';
import { ComplexRegister } from '../../../models/complex.model';
import { ComplexPictureRegister } from '../../../models/complex_picture.model';

type Props = {
    complex: ComplexRegister;
    addPicture: () => void;
    deletePicture: (id: number) => void;
    onChangePicture: (e: React.ChangeEvent<HTMLInputElement>, id: number) => void;
    formIsValid: boolean;
};

export default function FormPhotos({ complex, addPicture, deletePicture, onChangePicture, formIsValid }: Props): JSX.Element {
    return (
        <>
            <div className="pt-8">
                <div className="sm:flex sm:items-center">
                    <div className="sm:flex-auto">
                        <h1 className="text-xl font-semibold text-gray-900">Photos</h1>
                        <p className="mt-1 text-sm text-gray-500">Photographies de vos différents terrains</p>
                    </div>
                </div>
                <div className="my-6">
                    <div
                        onClick={addPicture}
                        className="cursor-pointer inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto"
                    >
                        Ajouter une photo
                    </div>
                </div>
                <div className="">
                    <div className="grid grid-cols-1 gap-4 sm:grid-cols-2">
                        {complex.complexPictures.map((picture: ComplexPictureRegister, i: number) => (
                            <div
                                key={picture.key}
                                className="relative rounded-lg border border-gray-300 bg-gray-100 px-6 py-5 shadow-sm flex items-center space-x-3  focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-transparent"
                            >
                                <div className="mt-1 flex rounded-md w-full items-center">
                                    <div className="flex flex-row w-full">
                                        <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">URL</span>
                                        <input
                                            type="text"
                                            value={picture.url}
                                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangePicture(e, picture.key)}
                                            id={`picture_${i}`}
                                            className={
                                                (picture.url !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                                ' flex-1 min-w-0 block w-full px-3 py-2 rounded-none sm:text-sm rounded-r-md'
                                            }
                                            placeholder="https://"
                                        />
                                    </div>
                                    <TrashIcon onClick={() => deletePicture(picture.key)} className="h-6 w-6 text-red-500 ml-4 cursor-pointer" />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}
