type Props = {
    textColor: string;
    backgroundColor: string;
    label: string;
};

export default function Badge({ textColor, backgroundColor, label }: Props): JSX.Element {
    return <span className={textColor + ' ' + backgroundColor + ' inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none border-gray-900 rounded-full'}>{label}</span>;
}
