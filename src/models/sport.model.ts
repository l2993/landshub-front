import Land from './land.model';

export default interface Sport {
    id: number;
    name: string;
    description?: string;
    lands?: Array<Land>;
}
