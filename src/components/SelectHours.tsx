type Props = {
    onChangeSchedule?: (e: React.ChangeEvent<HTMLSelectElement>) => void;
};

export default function SelectHours({ onChangeSchedule }: Props): JSX.Element {
    return (
        <select onChange={onChangeSchedule} id="hours" name="hours" className=" w-full text-base border-gray-300 focus:outline-none focus:ring-primary focus:border-primary focus:ring-1 sm:text-sm rounded-md">
            <option value={0}>00h</option>
            <option value={1}>01h</option>
            <option value={2}>02h</option>
            <option value={3}>03h</option>
            <option value={4}>04h</option>
            <option value={5}>05h</option>
            <option value={6}>06h</option>
            <option value={7}>07h</option>
            <option value={8}>08h</option>
            <option value={9}>09h</option>
            <option value={10}>10h</option>
            <option value={11}>11h</option>
            <option value={12}>12h</option>
            <option value={13}>13h</option>
            <option value={14}>14h</option>
            <option value={15}>15h</option>
            <option value={16}>16h</option>
            <option value={17}>17h</option>
            <option value={18}>18h</option>
            <option value={19}>19h</option>
            <option value={20}>20h</option>
            <option value={21}>21h</option>
            <option value={22}>22h</option>
            <option value={23}>23h</option>
        </select>
    );
}
