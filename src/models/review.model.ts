import User from './user.model';

export default interface Review {
    id: number;
    rate: number;
    message: string;
    reviewer: User;
}
