import { StarIcon as StarIconOutline, ChevronRightIcon } from '@heroicons/react/outline';
import { StarIcon as StarIconSolid } from '@heroicons/react/solid';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Complex from '../../models/complex.model';
import Land from '../../models/land.model';
import Review from '../../models/review.model';
import Badge from '../Badge';
import { isManager } from '../../functions/Authentication';

export default function ComplexCard({ complex }: { complex: Complex }): JSX.Element {
    const [rate, setRate] = useState<number>(0);
    const [sports, setSports] = useState<string[]>([]);

    useEffect(() => {
        calculRate();
        getSports();
    }, []);

    const calculRate = (): void => {
        if (!complex.reviews.length) {
            setRate(0);
        } else {
            const rate = complex.reviews?.reduce((acc: number, val: Review) => acc + val.rate, 0) / complex.reviews.length;
            setRate(rate);
        }
    };

    const getSports = (): void => {
        const sports = complex.lands.reduce((acc: string[], val: Land) => {
            acc.push(val.sport?.name);
            return acc;
        }, []);
        setSports(sports);
    };

    return (
        <div className="flex flex-col border border-gray-200 bg-gray-100 rounded-xl shadow-md p-4">
            <div className="-mx-4 -mt-4">
                <img src={complex.logo} className="object-cover w-full h-32 rounded-t-xl"></img>
            </div>
            <div className="flex flex-col gap-2 mt-2">
                <h2 className="text-primary text-xl font-bold">{complex.name}</h2>
                <ul className="flex items-center gap-x-1">
                    {[1, 2, 3, 4, 5].map((index: number) => {
                        return <li key={`${complex}_${index}`}>{rate >= index - 0.5 ? <StarIconSolid className="h-6 w-auto text-yellow-400" /> : <StarIconOutline className="h-6 w-auto text-yellow-400" />}</li>;
                    })}
                </ul>
                <div className="flex flex-row gap-1">
                    {sports?.map((sport: string) => {
                        return <Badge key={`${complex}_${sport}`} textColor={'text-white'} backgroundColor={'bg-primary'} label={sport} />;
                    })}
                </div>
                <p>{complex.description.slice(0, 170)} ...</p>
            </div>
            <div className="flex flex-row h-full justify-between mt-2">
                <div className="flex self-start h-full">
                    {isManager() && (
                        <Link to={`./${complex.id}/edit`} className="flex self-end">
                            <div className="cursor-pointer inline-flex items-center justify-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 sm:w-auto">
                                Modifier
                            </div>
                        </Link>
                    )}
                </div>
                <div className="flex self-end h-full">
                    <Link to={'/complexes/' + complex.id} className="flex self-end">
                        <ChevronRightIcon className="w-8 text-primary rounded-full border-primary border-2" />
                    </Link>
                </div>
            </div>
        </div>
    );
}
