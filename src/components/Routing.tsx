import { BrowserRouter, Route, Routes } from 'react-router-dom';

import App from '../App';
import Login from '../pages/auth/Login';
import Register from '../pages/auth/Register';
import ComplexDetail from '../pages/complexes/ComplexDetail';
import ComplexList from '../pages/complexes/list/ComplexList';
import Form from '../pages/complexes/form/Form';
import LandsBooking from '../pages/bookings/landsbooking';
import ListPlayers from '../pages/user/ListPlayers';
import Profile from '../pages/user/profile/Profile';
import ProfileEdit from '../pages/user/profile/ProfileEdit';
// import your route components too
import ManagerRoute from './specialsRoutes/ManagerRoute';
import NotFound from './NotFound';

export default function Routing(): JSX.Element {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<App />}>
                    <Route index element={<ComplexList />} />
                    <Route path="login" element={<Login />} />
                    <Route path="register" element={<Register />} />
                    <Route path="landsbooking/:landId" element={<LandsBooking />} />
                    {/* <Route path="complexes" element={<ComplexList />}> */}
                    <Route path="profile" element={<Profile />} />
                    <Route path="profile/:id" element={<Profile />} />
                    <Route path="profile/edit" element={<ProfileEdit />} />
                    <Route path="complexes" element={<ComplexList />} />
                    <Route path="complexes/:complexId" element={<ComplexDetail />} />
                    <Route path="complexes/new" element={<Form />} />
                    <Route
                        path="complexes/:id/edit"
                        element={
                            <ManagerRoute>
                                <Form />
                            </ManagerRoute>
                        }
                    />
                    <Route path="players" element={<ListPlayers />} />
                    <Route path="*" element={<NotFound />} />
                </Route>
            </Routes>
        </BrowserRouter>
    );
}
