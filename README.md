# LandsHub - Front :fire:

**LandsHub** est une application web permettant de pouvoir réserver des terrains adaptés à divers sports. Choisissez votre **sport**, votre **salle** et vos **horaires** et c’est parti ! :muscle:

# :cd: Installation :cd:

### Git

Cloner le repository :
> **Note:** Ce projet est en privé, il se peut que vous n'ayez pas droits pour le cloner.
```
git clone https://gitlab.com/l2993/landshub-front.git
```

### Commandes
> **Note:** Assurez vous d'être dans le dossier du projet pour effectuer les commandes suivantes.

Installation:
```
npm install
```

To Start Server:
```
npm start
```

## :open_file_folder: Démarrer l'application web :open_file_folder:

Pour lancer l'application web, exécutez la commande suivante :
> **Note:** Assurez vous d'être dans le dossier du projet pour effectuer les commandes suivantes.
```
localhost:3000
```
> **Note:** Vérifier que le port 3000 ne soit pas déjà utilisé par un autre service
