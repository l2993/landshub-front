import { AuthContext, AuthContextType } from './auth.context';
import Booking, { BookingRegister } from '../models/booking.model';
import React, { ReactNode, useContext } from 'react';

export type BookingContextType = {
    getBookingsOfALandAndOfADay: (page: number, date: Date, land: string) => Promise<{ bookings: Booking[]; totalCount: number } | null>;
    createBooking: (booking: BookingRegister) => Promise<Booking | null>;
};

export const BookingContext = React.createContext<BookingContextType | null>(null);

export const BookingProvider = ({ children }: { children: ReactNode }) => {
    const { token } = useContext(AuthContext) as AuthContextType;

    const getBookingsOfALandAndOfADay = async (page: number, date: Date, land: string): Promise<{ bookings: Booking[]; totalCount: number } | null> => {
        try {
            const url = new URL(`${process.env.REACT_APP_API_URL}/bookings`);
            // set up filter by date
            if (date) {
                url.searchParams.append('date', date.toLocaleDateString());
            }
            url.searchParams.append('land', land);
            url.searchParams.append('page', page.toString());

            const response: Response = await fetch(url.toString(), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            });

            if (!response.ok) {
                throw Error(response.statusText);
            } else {
                const json = await response.json();

                return json;
            }
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    const createBooking = async (booking: BookingRegister): Promise<Booking | null> => {
        try {
            const url = new URL(`${process.env.REACT_APP_API_URL}/bookings`);

            const response: Response = await fetch(url.toString(), {
                body: JSON.stringify(booking),
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                method: 'POST',
            });

            const json = await response.json();

            if (json.code && json.code === 401) {
                return null;
            } else {
                return json;
            }
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    return (
        <BookingContext.Provider
            value={{
                getBookingsOfALandAndOfADay,
                createBooking,
            }}
        >
            {children}
        </BookingContext.Provider>
    );
};
