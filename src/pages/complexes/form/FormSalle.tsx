import { ComplexRegister } from '../../../models/complex.model';
import { Switch } from '@headlessui/react';

type Props = {
    complex: ComplexRegister;
    setComplex: (complex: ComplexRegister) => void;
    toggleChangingRoom: () => void;
    isValideCoord: (coord: string, arg: string) => string;
    isValidLatitude: boolean;
    isValidLongitude: boolean;
    formIsValid: boolean;
    isEdit: boolean;
};

export default function FormSalle({ complex, setComplex, toggleChangingRoom, isValideCoord, isValidLatitude, isValidLongitude, formIsValid, isEdit }: Props): JSX.Element {
    return (
        <>
            <div>
                <div>
                    <h3 className="text-lg leading-6 font-medium text-gray-900">{isEdit ? 'Modifier' : 'Ajouter'} un complexe</h3>
                </div>
                <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
                    <div className="sm:col-span-3">
                        <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                            Nom du complexe
                        </label>
                        <div className="mt-1">
                            <input
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, name: e.target.value })}
                                value={complex.name}
                                type="text"
                                id="name"
                                className={
                                    (complex.name !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                    ' shadow-sm block w-full sm:text-sm  rounded-md'
                                }
                            />
                        </div>
                    </div>
                    <div className="sm:col-span-3">
                        <label htmlFor="logo" className="block text-sm font-medium text-gray-700">
                            Logo du complexe
                        </label>
                        <div className="mt-1 sm:col-span-2">
                            <div className="max-w-lg flex rounded-md shadow-sm">
                                <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">URL</span>
                                <input
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, logo: e.target.value })}
                                    type="text"
                                    value={complex.logo}
                                    id="logo"
                                    className={
                                        (complex.logo !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                        ' flex-1 block w-full min-w-0 rounded-none rounded-r-md sm:text-sm'
                                    }
                                    placeholder="https://"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="sm:col-span-3 sm:row-start-2">
                        <label htmlFor="description" className="block text-sm font-medium text-gray-700">
                            Description
                        </label>
                        <div className="mt-1">
                            <textarea
                                onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) => setComplex({ ...complex, description: e.target.value })}
                                id="description"
                                value={complex.description}
                                rows={3}
                                className={
                                    (complex.description !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                    ' shadow-sm block w-full sm:text-sm border rounded-md'
                                }
                                defaultValue={''}
                            />
                        </div>
                    </div>

                    <div className="sm:col-span-3 sm:row-start-2">
                        <Switch.Group as="div" className="flex items-center h-full ml-8">
                            <Switch
                                checked={complex.hasChangingRoom}
                                onChange={toggleChangingRoom}
                                className={
                                    (complex.hasChangingRoom ? 'bg-indigo-600 ' : 'bg-gray-200 ') +
                                    ' relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-transparent'
                                }
                            >
                                <span
                                    aria-hidden="true"
                                    className={(complex.hasChangingRoom ? 'translate-x-5 ' : 'translate-x-0 ') + ' pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200'}
                                />
                            </Switch>
                            <Switch.Label as="span" className="ml-3">
                                <span className="text-sm font-medium text-gray-900">Possède des vestiaires</span>
                            </Switch.Label>
                        </Switch.Group>
                    </div>

                    <div className="sm:col-span-2 sm:row-start-3">
                        <label htmlFor="address" className="block text-sm font-medium text-gray-700">
                            Adresse
                        </label>
                        <div className="mt-1">
                            <input
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, address: e.target.value })}
                                type="text"
                                value={complex.address}
                                id="address"
                                className={
                                    (complex.address !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                    ' shadow-sm block w-full sm:text-sm rounded-md'
                                }
                            />
                        </div>
                    </div>

                    <div className="sm:col-span-1 sm:row-start-3">
                        <label htmlFor="city" className="block text-sm font-medium text-gray-700">
                            Ville
                        </label>
                        <div className="mt-1">
                            <input
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, city: e.target.value })}
                                type="text"
                                value={complex.city}
                                id="city"
                                className={
                                    (complex.city !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                    ' shadow-sm block w-full sm:text-sm rounded-md'
                                }
                            />
                        </div>
                    </div>

                    <div className="sm:col-span-1 sm:row-start-3">
                        <label htmlFor="postal" className="block text-sm font-medium text-gray-700">
                            Code postal
                        </label>
                        <div className="mt-1">
                            <input
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, postal: e.target.value })}
                                type="text"
                                value={complex.postal}
                                id="postal"
                                className={
                                    (complex.postal !== '' || formIsValid ? 'focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 ' : ' focus:ring-red-500 focus:border-red-500 border-red-500 ') +
                                    ' shadow-sm block w-full sm:text-sm rounded-md'
                                }
                            />
                        </div>
                    </div>

                    <div className="sm:col-span-2 sm:row-start-4">
                        <div>
                            <label htmlFor="coordinate" className="block text-sm font-medium text-gray-700">
                                Coordonnées
                            </label>
                            <div className="mt-1 flex rounded-md shadow-sm">
                                <span className="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">Latitude</span>
                                <input
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, latitude: isValideCoord(e.target.value, 'latitude') })}
                                    type="text"
                                    pattern="-?[0-9]{1,3}[.][0-9]+"
                                    value={complex.latitude}
                                    id="latitude"
                                    className={
                                        (complex.latitude === ''
                                            ? 'border-gray-300 focus:ring-indigo-500 focus:border-indigo-500'
                                            : isValidLatitude
                                            ? 'border-green-500 focus:ring-green-500 focus:border-green-500 '
                                            : 'border-red-500 focus:ring-red-500 focus:border-red-500 ') + ' flex-1 min-w-0 block w-full px-3 py-2 rounded-none rounded-r-md  sm:text-sm border-gray-300'
                                    }
                                    placeholder="44.8765411"
                                />
                                <span className="inline-flex items-center px-3 border border-r-0 border-l-0 border-gray-300 bg-gray-50 text-gray-500 sm:text-sm">Longitude</span>
                                <input
                                    type="text"
                                    id="lagitude"
                                    pattern="-?[0-9]{1,3}[.][0-9]+"
                                    value={complex.longitude}
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComplex({ ...complex, longitude: isValideCoord(e.target.value, 'longitude') })}
                                    className={
                                        (complex.longitude === ''
                                            ? 'border-gray-300 focus:ring-indigo-500 focus:border-indigo-500 '
                                            : isValidLongitude
                                            ? 'border-green-500 focus:ring-green-500 focus:border-green-500 '
                                            : 'border-red-500 focus:ring-red-500 focus:border-red-500 ') + ' flex-1 min-w-0 block w-full px-3 py-2 rounded-none rounded-r-md  sm:text-sm border-gray-300'
                                    }
                                    placeholder="-0.6868031"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
