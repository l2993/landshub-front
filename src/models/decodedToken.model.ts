export default interface DecodedToken {
    id?: number;
    exp: number;
    iat: number;
    roles: string[];
    username: string;
}

export const emptyDecodedToken: DecodedToken = {
    exp: 0,
    iat: 0,
    roles: [],
    username: '',
};
