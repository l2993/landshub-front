import Lottie from 'lottie-react';
import Lottie404 from '../lotties/404.json';
import LottieParachute from '../lotties/parachute.json';

export default function NotFound(): JSX.Element {
    return (
        <div>
            <Lottie style={{ height: '400px', marginTop: '0px' }} autoPlay={true} loop={true} animationData={Lottie404} />
            <Lottie style={{ height: '350px', marginTop: '-120px' }} autoPlay={true} loop={true} animationData={LottieParachute} />
        </div>
    );
}
