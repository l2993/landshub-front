import React, { useContext, useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { UserLogin } from '../../models/user.model';
import { AuthContext, AuthContextType } from '../../contexts/auth.context';
import Toastr from '../../components/Toastr';

export default function Login(): JSX.Element {
    const navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem('token')) {
            navigate('/');
        }
    }, []);

    const [user, setUser] = useState<UserLogin>({
        username: '',
        password: '',
    });
    const [showToastr, setShowToastr] = useState<boolean>(false);
    const [toastr, setToastr] = useState<{ type: string; title: string; message: string }>({ type: '', title: '', message: '' });

    const { login } = useContext(AuthContext) as AuthContextType;

    const onSubmit = async (): Promise<void> => {
        if (formIsValid()) {
            const res = await login(user);
            if (res.token) {
                setToastr({ type: 'success', title: 'Connexion réussie', message: 'Vous vous êtes bien connecté' });
                setShowToastr(true);
                setTimeout(() => {
                    setShowToastr(false);
                    navigate('/complexes');
                }, 1500);
            } else {
                setToastr({ type: 'error', title: 'La connexion à échouée', message: 'Une erreur est survenue' });
                setShowToastr(true);
                setTimeout(() => setShowToastr(false), 3000);
            }
        }
    };

    const formIsValid = (): boolean => {
        const { username, password } = user;
        return username.length > 0 && password.length > 0;
    };

    return (
        <>
            {showToastr && <Toastr type={toastr.type} title={toastr.title} message={toastr.message} />}
            <div className="mx-auto px-4 h-full my-6">
                <div className="flex content-center items-center justify-center h-full">
                    <div className="w-full lg:w-8/12 px-4">
                        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                            <div className="rounded-t mb-0 px-6 py-6">
                                <div className="text-center mb-3">
                                    <h2 className="text-primary text-2xl font-bold">Connexion</h2>
                                </div>
                                <form className="flex flex-col space-y-3">
                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="email">
                                            Adresse email
                                        </label>
                                        <input
                                            id="email"
                                            type="email"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="test@latest.com"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, username: event.target.value })}
                                        />
                                    </div>

                                    <div className="relative w-full">
                                        <label className="block uppercase text-xs font-bold mb-2" htmlFor="password">
                                            Mot de passe
                                        </label>
                                        <input
                                            id="password"
                                            type="password"
                                            className="border-0 px-3 py-3 placeholder-gray-400 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                            placeholder="Password"
                                            style={{ transition: 'all .15s ease' }}
                                            required
                                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUser({ ...user, password: event.target.value })}
                                        />
                                    </div>
                                    <div className="text-center mt-6 mb-2">
                                        <button
                                            className="bg-primary text-white text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                            type="button"
                                            style={{ transition: 'all .15s ease' }}
                                            onClick={onSubmit}
                                        >
                                            Me connecter
                                        </button>
                                    </div>
                                </form>
                                <div className="mt-2 col-span-2">
                                    <div className="relative">
                                        <div className="absolute inset-0 flex items-center">
                                            <div className="w-full border-t border-black" />
                                        </div>
                                        <div className="relative flex justify-center text-sm">
                                            <span className="px-2 bg-gray-300 text-black-500">Ou</span>
                                        </div>
                                    </div>
                                </div>

                                <Link to={'/register'} className="col-span-2 mt-2">
                                    <div className="text-center mt-6">
                                        <div className="bg-primary text-white text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full" style={{ transition: 'all .15s ease' }}>
                                            M&apos;inscrire
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
