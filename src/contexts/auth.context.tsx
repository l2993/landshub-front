import React, { ReactNode, useEffect, useState } from 'react';
import User, { UserLogin, UserRegister } from '../models/user.model';

export type AuthContextType = {
    login: (user: UserLogin) => Promise<{ token: string }>;
    register: (user: UserRegister) => Promise<User>;
    logout: () => void;
    token: string;
};

export const AuthContext = React.createContext<AuthContextType | null>(null);

export const AuthProvider = ({ children }: { children: ReactNode }) => {
    const [token, setToken] = useState<string>(localStorage.getItem('token') ?? '');

    const login = async (user: UserLogin): Promise<{ token: string }> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/login_check`, {
                body: JSON.stringify(user),
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            const json: { token: string } = await response.json();
            setToken(json.token);
            localStorage.setItem('token', json.token);

            return json;
        } catch (error: any) {
            console.error(error);
            return error;
        }
    };

    const register = async (user: UserRegister): Promise<User> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/users`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: user.email,
                    password: user.password,
                    firstname: user.firstName,
                    lastname: user.lastName,
                    isPublic: user.isPublic,
                    phone: user.phone,
                    roles: user.roles,
                    avatarUrl: `https://avatars.dicebear.com/api/micah/${user.firstName}.svg`,
                }),
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            const json: User = await response.json();

            return json;
        } catch (error: any) {
            console.error(error);
            return error;
        }
    };

    const logout = (): void => {
        setToken('');
        localStorage.removeItem('token');
    };

    useEffect(() => {
        if (localStorage.getItem('token')) {
            setToken(localStorage.getItem('token') ?? '');
        }
    }, []);

    return (
        <AuthContext.Provider
            value={{
                login,
                register,
                logout,
                token,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};
