import { Navigate } from 'react-router-dom';
import { isManager } from '../../functions/Authentication';

export default function ManagerRoute({ children }: { children: JSX.Element }) {
    return isManager() ? children : <Navigate to="/complexes" />;
}
