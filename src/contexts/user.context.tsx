import jwtDecode from 'jwt-decode';
import React, { ReactNode, useContext } from 'react';
import DecodedToken from '../models/decodedToken.model';
import User, { ProfileRegister } from '../models/user.model';
import { AuthContext, AuthContextType } from './auth.context';

export type UserContextType = {
    getPlayers: (page: number, search: string, sports: { value: number; label: string }[]) => Promise<{ users: User[]; totalCount: number } | null>;
    getProfile: () => Promise<User | null>;
    getUser: (userId: string) => Promise<User | null>;
    editProfile: (profile: ProfileRegister) => Promise<User | null>;
    deleteProfile: () => Promise<boolean>;
};

export const UserContext = React.createContext<UserContextType | null>(null);

export const UserProvider = ({ children }: { children: ReactNode }) => {
    const { token } = useContext(AuthContext) as AuthContextType;

    const getProfile = async (): Promise<User | null> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/profile`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            });

            const json: User = await response.json();

            return json;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    const getUser = async (userId: string): Promise<User | null> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            });

            const json: User = await response.json();

            return json;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    const getPlayers = async (page: number, search: string, sports: { value: number; label: string }[]): Promise<{ users: User[]; totalCount: number } | null> => {
        try {
            const url = new URL(`${process.env.REACT_APP_API_URL}/users?isPublic=1`);
            if (search !== '') url.searchParams.append('search', search);
            if (sports.length) {
                sports.forEach(({ value }: { value: number }) => {
                    url.searchParams.append('sports[]', value.toString());
                });
            }
            url.searchParams.append('page', page.toString());
            const response: Response = await fetch(url.toString(), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            });
            const json: { users: User[]; totalCount: number } = await response.json();

            return json;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    const editProfile = async (profile: ProfileRegister): Promise<User | null> => {
        try {
            const decodedToken: DecodedToken = jwtDecode(token);
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/users/${decodedToken.id}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(profile),
            });
            const json: User = await response.json();

            if (!json.id) {
                return null;
            }

            return json;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    const deleteProfile = async (): Promise<boolean> => {
        try {
            const decodedToken: DecodedToken = jwtDecode(token);
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/users/${decodedToken.id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                method: 'DELETE',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }
            if (response.status === 204) {
                return true;
            }
            return false;
        } catch (error: any) {
            console.error('error', error);
            return error;
        }
    };

    return (
        <UserContext.Provider
            value={{
                getProfile,
                getUser,
                getPlayers,
                editProfile,
                deleteProfile,
            }}
        >
            {children}
        </UserContext.Provider>
    );
};
