/*
    A utiliser avec le hook UsePagination qui permet d'obtenir tout les parametres utiles au fonctionnement de cette page
*/

/*
    currentPage => (int)        => Page actuelle
    next        => (function)   => Aller à la page suivante
    prev        => (function)   => Aller à la page précédente
    jump(x)     => (function)   => Aller à une page x
    paginate    => (boolean)    => Pagination necessaire
    maxPage     => (int)        => Page maximum

*/
type Props = {
    currentPage: number;
    next: () => void;
    prev: () => void;
    jump: (pageNumber: number) => void;
    paginate: boolean;
    maxPage: number;
};
export default function Pagination({ currentPage = 1, next, prev, jump, paginate = false, maxPage }: Props): JSX.Element {
    if (!paginate) {
        return <></>;
    }

    return (
        <div className="flex flex-row justify-center mt-2">
            <div className="py-2">
                {paginate && currentPage === 1 && currentPage !== maxPage ? (
                    <nav className="block">
                        <ul className="flex pl-0 rounded list-none flex-wrap">
                            <li>
                                <a
                                    href="#"
                                    className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-primary text-white hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                                >
                                    {currentPage}
                                </a>
                            </li>
                            <li>
                                <a
                                    href="#"
                                    onClick={next}
                                    className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                                >
                                    {currentPage + 1}
                                </a>
                            </li>

                            <a
                                href="#"
                                onClick={next}
                                className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                                aria-label="Next"
                            >
                                <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                                </svg>
                            </a>
                            <a
                                href="#"
                                onClick={() => jump(maxPage)}
                                className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                                aria-label="Next"
                            >
                                Fin
                            </a>
                        </ul>
                    </nav>
                ) : null}

                {paginate && currentPage > 1 && currentPage < maxPage ? (
                    <nav className="cursor-pointer relative z-0 inline-flex shadow-sm">
                        <a
                            href="#"
                            onClick={() => jump(1)}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                            aria-label="Previous"
                        >
                            Début
                        </a>
                        <a
                            href="#"
                            onClick={prev}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                            </svg>
                        </a>
                        <a
                            href="#"
                            onClick={prev}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            {currentPage - 1}
                        </a>
                        <a
                            href="#"
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-primary text-white hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            {currentPage}
                        </a>
                        <a
                            href="#"
                            onClick={next}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            {currentPage + 1}
                        </a>
                        <a
                            href="#"
                            onClick={next}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
                            </svg>
                        </a>
                        <a
                            href="#"
                            onClick={() => jump(maxPage)}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                            aria-label="Next"
                        >
                            Fin
                        </a>
                    </nav>
                ) : null}

                {paginate && currentPage === maxPage ? (
                    <nav className="cursor-pointer relative z-0 inline-flex shadow-sm">
                        <a
                            href="#"
                            onClick={() => jump(1)}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                            aria-label="Previous"
                        >
                            Début
                        </a>
                        <a
                            href="#"
                            onClick={prev}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
                            </svg>
                        </a>
                        <a
                            href="#"
                            onClick={prev}
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-white text-primary hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            {currentPage - 1}
                        </a>
                        <a
                            href="#"
                            className="first:ml-0 text-xs font-semibold flex w-10 h-10 mx-1 p-0 rounded-full items-center justify-center leading-tight relative border border-solid border-primary bg-primary text-white hover:text-primary hover:border-primary focus:z-10 focus:outline-none focus:border-primary focus:shadow-outline-primary active:bg-primary active:text-white transition ease-in-out duration-150"
                        >
                            {currentPage}
                        </a>
                    </nav>
                ) : null}
            </div>
        </div>
    );
}
