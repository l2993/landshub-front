import React, { ReactNode, useContext } from 'react';
import Complex, { ComplexRegister } from '../models/complex.model';
import { AuthContext, AuthContextType } from './auth.context';

export type ComplexContextType = {
    getComplex: (idComplex: string) => Promise<Complex | null>;
    createComplex: (complex: ComplexRegister) => Promise<Complex>;
    getComplexEdit: (id: string) => Promise<ComplexRegister>;
    editComplex: (complex: ComplexRegister) => Promise<Complex>;
    getComplexes: (page: number, search: string, sports: { value: number; label: string }[], hasChangingRoom: boolean, city?: string) => Promise<{ complexes: Complex[]; totalCount: number } | null>;
    deleteComplex: (id: string | undefined) => Promise<boolean>;
};

export const ComplexContext = React.createContext<ComplexContextType | null>(null);

export const ComplexProvider = ({ children }: { children: ReactNode }) => {
    const { token } = useContext(AuthContext) as AuthContextType;

    const createComplex = async (complex: ComplexRegister): Promise<Complex> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/complexes`, {
                body: JSON.stringify(complex),
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                method: 'POST',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            const res: Complex = await response.json();

            return res;
        } catch (error: any) {
            console.error('error', error);
            return error;
        }
    };

    const getComplex = async (id: string): Promise<Complex> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/complexes/${id}`, {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'GET',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            const res: Complex = await response.json();

            return res;
        } catch (error: any) {
            console.error('error', error);
            return error;
        }
    };

    const getComplexEdit = async (id: string): Promise<ComplexRegister> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/complexes/${id}`, {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'GET',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            const res: ComplexRegister = await response.json();

            return res;
        } catch (error: any) {
            console.error('error', error);
            return error;
        }
    };

    const editComplex = async (complex: ComplexRegister): Promise<Complex> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/complexes/${complex.id}`, {
                body: JSON.stringify(complex),
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                method: 'PUT',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            const res: Complex = await response.json();

            return res;
        } catch (error: any) {
            console.error('error', error);
            return error;
        }
    };

    const getComplexes = async (page: number, search: string, sports: { value: number; label: string }[], hasChangingRoom: boolean, city?: string): Promise<{ complexes: Complex[]; totalCount: number } | null> => {
        try {
            const url = new URL(`${process.env.REACT_APP_API_URL}/complexes`);
            // set up filters
            if (search !== '') url.searchParams.append('name', search);
            if (sports.length) {
                sports.forEach(({ value }: { value: number }) => {
                    url.searchParams.append('sports[]', value.toString());
                });
            }
            if (hasChangingRoom) url.searchParams.append('hasChangingRoom', JSON.stringify(hasChangingRoom));
            if (city) url.searchParams.append('city', city);
            url.searchParams.append('page', page.toString());

            const response: Response = await fetch(url.toString(), {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            });

            const json: { complexes: Complex[]; totalCount: number } = await response.json();

            return json;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    };

    const deleteComplex = async (id: string | undefined): Promise<boolean> => {
        try {
            const response: Response = await fetch(`${process.env.REACT_APP_API_URL}/complexes/${id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                method: 'DELETE',
            });

            if (!response.ok) {
                throw Error(response.statusText);
            }

            if (response.status === 204) {
                return true;
            } else {
                return false;
            }
        } catch (error: any) {
            console.error('error', error);
            return error;
        }
    };

    return (
        <ComplexContext.Provider
            value={{
                getComplex,
                createComplex,
                getComplexEdit,
                editComplex,
                getComplexes,
                deleteComplex,
            }}
        >
            {children}
        </ComplexContext.Provider>
    );
};
