export default interface ComplexPicture {
    id: number;
    complex_id: number;
    url: string;
}

export interface ComplexPictureRegister {
    id?: number;
    key: number;
    url: string;
}
